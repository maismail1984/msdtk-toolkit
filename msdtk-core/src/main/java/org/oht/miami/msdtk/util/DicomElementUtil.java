/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdtk.util;

/*
 * This class complement the DicomElement interface. 
 * It has a set of functions to handle DicomElement
 * It can be added to the DicomElement interface, however we 
 * preferred not to change the implementation of the dcm4che2 library
 * */
import org.oht.miami.msdtk.studymodel.BulkDataDicomElement;

import org.dcm4che2.data.DicomElement;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.VR;

import java.util.Iterator;

/**
 * Utilities class to process dcm4che2.0 Dicom elements.
 */
public class DicomElementUtil {

    /**
     * Compares the contents of 2 dicomElements, return true if equal
     * 
     * @param reference
     *            , First dicomElement.
     * @param newElement
     *            , Second dicomElement
     * @return boolean determines if the inputs are equal or not.
     */
    public static boolean compareDicomElements(final DicomElement reference,
            final DicomElement newElement) {

        if (reference.countItems() != newElement.countItems()) {
            return false;
        }
        // If both are simple dicom elements
        if (!reference.hasDicomObjects()) {
            if (reference instanceof BulkDataDicomElement
                    && newElement instanceof BulkDataDicomElement) {
                return (((BulkDataDicomElement) reference).getBulkDataValueLength() == ((BulkDataDicomElement) newElement)
                        .getBulkDataValueLength())
                        && (((BulkDataDicomElement) reference).getBulkDataVR() == ((BulkDataDicomElement) newElement)
                                .getBulkDataVR());
            } else {
                try {
                    return java.util.Arrays.equals(reference.getBytes(), newElement.getBytes());
                } catch (Exception ex) {
                    System.out.println(reference +"\n" + newElement );
                }
            }
        }
        for (int i = 0; i < reference.countItems(); i++) {
            DicomObject objRef = reference.getDicomObject(i);
            DicomObject objNew = newElement.getDicomObject(i);
            if (objRef.size() != objNew.size()) {
                return false;
            }
            Iterator<DicomElement> iterRef = objRef.iterator();
            Iterator<DicomElement> iterNew = objNew.iterator();
            while (iterRef.hasNext()) {
                if (!compareDicomElements(iterRef.next(), iterNew.next())) {
                    return false;
                }
            }
        }
        return true;
    }

    public static int getCount(DicomElement dcmElem) {
        int count = 1;
        if (dcmElem.vr() == VR.SQ) {
            count = 0;
            for (int i = 0; i < dcmElem.countItems(); i++) {
                DicomObject childObj = dcmElem.getDicomObject(i);
                /* Add Attributes */
                for (Iterator<DicomElement> iter = childObj.iterator(0x00020000, 0xffffffff); iter
                        .hasNext();) {
                    count += getCount(iter.next());
                }
            }
        }
        return count;
    }
}
