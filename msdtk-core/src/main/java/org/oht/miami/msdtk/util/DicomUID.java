/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdtk.util;

import org.dcm4che2.util.UIDUtils;

import java.io.Serializable;

/**
 * This class is a type wrapper for DICOM UIDs
 * 
 * @author James F Philbin, Josh Stephens (joshua.stephens@harris.com)
 */

public class DicomUID implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -3558525133787041743L;
    private final String dicomUID;

    /**
     * Checks if the siuid parameter is a valid UID.
     * 
     * @param siuid
     *            A DICOM UID string
     * @return true if the siuid is a valid UID.
     */
    public static boolean validStudyInstanceUID(String siuid) {
        return UIDUtils.isValidUID(siuid);
    }

    /**
     * Creates a new randomly generated DicomUID.
     * 
     * @throws IllegalArgumentException
     *             Thrown if the uid parameter is not a valid UID.
     */
    public DicomUID() {
        dicomUID = UIDUtils.createUID();
    }

    /**
     * Creates a DICOM DicomUID - checks that the syntax of the string is valid.
     * 
     * @param uid
     *            A DICOM UID string in valid format
     * @throws IllegalArgumentException
     *             Thrown if the uid parameter is not a valid UID.
     */
    public DicomUID(String uid) throws IllegalArgumentException {

        if (!validStudyInstanceUID(uid)) {
            throw new IllegalArgumentException("Illegal Study Instance UID:  " + uid);
        }
        dicomUID = uid;
    }

    @Override
    public String toString() {
        return dicomUID;
    }

    @Override
    public int hashCode() {
        return dicomUID == null ? 0 : dicomUID.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof DicomUID)) {
            return false;
        }
        DicomUID other = (DicomUID) obj;
        return dicomUID.equals(other.dicomUID);
    }

}
