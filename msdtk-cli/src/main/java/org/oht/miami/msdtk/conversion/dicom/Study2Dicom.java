/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdtk.conversion.dicom;

import org.oht.miami.msdtk.store.BulkDataInfo;
import org.oht.miami.msdtk.store.BulkDataSet;
import org.oht.miami.msdtk.store.VersionHistory;
import org.oht.miami.msdtk.studymodel.Instance;
import org.oht.miami.msdtk.studymodel.Series;
import org.oht.miami.msdtk.studymodel.Study;

import org.dcm4che2.data.BasicDicomObject;
import org.dcm4che2.data.DicomElement;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.dcm4che2.data.TransferSyntax;
import org.dcm4che2.data.UID;
import org.dcm4che2.data.VR;
import org.dcm4che2.io.DicomOutputStream;
import org.dcm4che2.util.ByteUtils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Class to convert Study model to dcm4che DICOM objects. The class supports
 * single-frame DICOM, multi-frame DICOM and multi-series DICOM
 * 
 * @author Mahmoud Ismail(maismail@cs.jhu.edu)
 */
public class Study2Dicom {

    public static DicomObject study2UnifiedMultiSeriesDicom(Study study) throws IOException {

        if (study.getAttribute(Tag.TransferSyntaxUID) == null) {
            throw new IllegalArgumentException(
                    "Could not convert Study with mixed transfer syntaxes");
        }

        DicomObject studyDcmObj = getStudyLevelAttributesAsDicomObject(study);
        Iterator<Series> seriesIter = study.seriesIterator();

        // StudyInstanceUID is chosen to be the SOPInstanceUID of the created
        // DICOM object.
        studyDcmObj.putString(Tag.MediaStorageSOPInstanceUID, VR.UI,
                study.getStudyInstanceUIDAsString());
        studyDcmObj.putString(Tag.SOPInstanceUID, VR.UI, study.getStudyInstanceUIDAsString());

        /*
         * SOPClassUID has to be added. If it does not exit, add the default one
         * (secondary capture image storage)
         */
        if (study.getAttribute(Tag.SOPClassUID) == null) {
            studyDcmObj.putString(Tag.SOPClassUID, VR.UI, "1.2.840.10008.5.1.4.1.1.7");
            studyDcmObj.putString(Tag.MediaStorageSOPClassUID, VR.UI, "1.2.840.10008.5.1.4.1.1.7");
        }

        // Sequence DICOM object to hold all series within the study
        DicomElement seriesSequence = studyDcmObj
                .putSequence(Tag.PerSeriesFunctionalGroupsSequence);
        while (seriesIter.hasNext()) {
            Series series = seriesIter.next();
            ArrayList<DicomObject> seriesDicomObjects = Series2Dicom.series2MultiFrameDicom(series,
                    null);
            for (DicomObject seriesDicomObject : seriesDicomObjects) {
                seriesSequence.addDicomObject(seriesDicomObject);
                seriesDicomObject.setParent(studyDcmObj);
            }
        }
        return studyDcmObj;
    }

    /**
     * Create a dicom object with study level attributes
     * 
     * @param study
     *            containing study level attributes
     * @return dicom object containing study level attributes
     */
    public static DicomObject getStudyLevelAttributesAsDicomObject(Study study) {

        // Creates a base object and adds study level attributes to it
        DicomObject studyDcmObj = new BasicDicomObject();
        Iterator<DicomElement> studyAttrIter = study.attributeIterator();
        while (studyAttrIter.hasNext()) {
            studyDcmObj.add(studyAttrIter.next());
        }
        return studyDcmObj;
    }

    /**
     * Create a multi series dicom object from a study
     * 
     * @param study
     *            to create the Multi Series dicom object from
     * @return Multi series dicom object
     * @throws IOException
     */
    public static DicomObject study2MultiSeriesDicom(Study study) throws IOException {
        return study2MultiSeriesDicom(study, null);
    }

    /**
     * Create a multi series dicom object from a study with current and previous
     * version history
     * 
     * @param study
     *            to create the Multi Series dicom object from
     * @param history
     *            VersionHistory for a study
     * @return Multi series dicom object
     * @throws IOException
     */
    public static DicomObject study2MultiSeriesDicom(Study study, VersionHistory history)
            throws IOException {
        DicomObject studyDicomObject = getStudyLevelAttributesAsDicomObject(study);
        addFileMetaElements(study, history, studyDicomObject);
        addDataElements(study, studyDicomObject);
        return studyDicomObject;

    }

    /**
     * Add data elements to a given dicom object
     * 
     * @param study
     *            object to pull fields from
     * @param studyDicomObject
     *            to put attributes into
     */
    private static void addDataElements(Study study, DicomObject studyDicomObject) {
        DicomElement perSeriesSq = studyDicomObject
                .putSequence(Tag.PerSeriesFunctionalGroupsSequence);
        Iterator<Series> seriesIter = study.seriesIterator();
        while (seriesIter.hasNext()) {
            Series series = seriesIter.next();
            DicomObject seriesDicomObject = new BasicDicomObject();
            perSeriesSq.addDicomObject(seriesDicomObject);
            Iterator<DicomElement> seriesAttributeIter = series.attributeIterator();
            while (seriesAttributeIter.hasNext()) {
                seriesDicomObject.add(seriesAttributeIter.next());
            }
            enforceExplicitVRLittleEndian(seriesDicomObject, false);

            DicomElement perInstanceSq = seriesDicomObject
                    .putSequence(Tag.PerInstanceFunctionalGroupsSequence);
            Iterator<Instance> instanceIter = series.instanceIterator();
            while (instanceIter.hasNext()) {
                Instance instance = instanceIter.next();
                DicomObject instanceDicomObject = new BasicDicomObject();
                perInstanceSq.addDicomObject(instanceDicomObject);
                Iterator<DicomElement> instanceAttributeIter = instance.attributeIterator();
                while (instanceAttributeIter.hasNext()) {
                    instanceDicomObject.add(instanceAttributeIter.next());
                }
                enforceExplicitVRLittleEndian(instanceDicomObject, false);

                if (instance.hasChildFrames()) {
                    DicomElement perFrameSq = instanceDicomObject
                            .putSequence(Tag.PerFrameFunctionalGroupsSequence);
                    for (Instance frame : instance.getChildrenFrames()) {
                        DicomObject frameDicomObject = new BasicDicomObject();
                        perFrameSq.addDicomObject(frameDicomObject);
                        Iterator<DicomElement> frameAttributeIter = frame.attributeIterator();
                        while (frameAttributeIter.hasNext()) {
                            frameDicomObject.add(frameAttributeIter.next());
                        }
                    }
                }
            }
        }
    }

    /**
     * Adds file meta information to the given dicom object from the study
     * 
     * @param study
     *            object to pull fields from
     * @param history
     *            VersionHistory to put into private information
     * @param studyDicomObject
     *            to put attributes into
     * @throws IOException
     */
    private static void addFileMetaElements(Study study, VersionHistory history,
            DicomObject studyDicomObject) throws IOException {

        // Convert Transfer Syntax to Explicit VR Little Endian, if necessary
        enforceExplicitVRLittleEndian(studyDicomObject, true);

        // Set File Meta Information Version to the version number of the MSD
        // File Meta Format, currently 0
        studyDicomObject.putBytes(Tag.FileMetaInformationVersion, VR.OB,
                ByteUtils.ushort2bytesBE(0, new byte[2], 0));

        // Set Media Storage SOP Class UID to a special UID for MSD
        studyDicomObject.putString(Tag.MediaStorageSOPClassUID, VR.UI, UID.MultiSeriesStudyStorage);

        // Set Media Storage SOP Instance UID to Study Instance UID
        studyDicomObject.putString(Tag.MediaStorageSOPInstanceUID, VR.UI,
                study.getStudyInstanceUIDAsString());

        // Set Implementation Class UID to a special UID for MSD-Toolkit
        studyDicomObject.putString(Tag.ImplementationClassUID, VR.UI,
                Study.IMPLEMENTATION_CLASS_UID);

        // Set Implementation Version Name to the version number of MSD-Toolkit
        studyDicomObject.putString(Tag.ImplementationVersionName, VR.SH,
                Study.IMPLEMENTATION_VERSION_NAME);
        // Put source application ae title as that of the MSD-Toolkit
        studyDicomObject.putString(Tag.SourceApplicationEntityTitle, VR.AE, Study.SOURCE_AET);
        // Set Private Information Creator UID to the MSD-Toolkit UID
        studyDicomObject.putString(Tag.PrivateInformationCreatorUID, VR.UI,
                Study.IMPLEMENTATION_CLASS_UID);
        addPrivateInformation(study, history, studyDicomObject);
    }

    private static void addPrivateInformation(Study study, VersionHistory versionHistory,
            DicomObject studyDicomObject) throws IOException {
        DicomObject privateInfoItem = new BasicDicomObject();

        if (versionHistory != null) {
            privateInfoItem.add(versionHistory.encodeVersionHistory());
        }

        encodeBulkDataSet(privateInfoItem, study.getBulkDataSet());

        encodePrivateInformation(privateInfoItem, studyDicomObject);
    }

    /**
     * Encode bulk data set to file meta information
     * 
     * @param privInfoObj
     *            dicom object to file meta information
     * @param bulkDataSet
     *            bulk data set to be added
     */
    private static void encodeBulkDataSet(DicomObject privInfoObj, BulkDataSet bulkDataSet) {
        // Bulk Data Sequence
        DicomElement bulkDataSeq = privInfoObj.putSequence(Tag.BulkDataSequence);
        for (BulkDataInfo bulkDataInfo : bulkDataSet) {
            DicomObject bulkDataItem = new BasicDicomObject();
            bulkDataItem.putString(Tag.BulkDataUUID, VR.LO, bulkDataInfo.getUUID().toString());
            bulkDataSeq.addDicomObject(bulkDataItem);
        }
    }

    /**
     * Add private information tags to a given dicom object
     * 
     * @param privInfoObj
     *            object that contains private information
     * @param dicomObj
     *            object private information will be written to
     * @throws IOException
     */
    private static void encodePrivateInformation(DicomObject privInfoObj, DicomObject dicomObj)
            throws IOException {
        // Encode Private Information into an Item
        ByteArrayOutputStream bytesOut = new ByteArrayOutputStream();
        DicomOutputStream dicomOut = new DicomOutputStream(bytesOut);
        try {
            dicomOut.writeItem(privInfoObj, TransferSyntax.ExplicitVRLittleEndian);
        } finally {
            dicomOut.close();
        }

        // Add Private Information
        dicomObj.putBytes(Tag.PrivateInformation, VR.OB, bytesOut.toByteArray());
    }

    /**
     * Enforce Explicit VR Little Endian transfer syntax if the transfer syntax
     * is either ExplicitVRBigEndian or ImplicitVRLittleEndian
     * 
     * @param obj
     *            A DICOM object that represents a study, a series, or an
     *            instance.
     * @param addIfNotExists
     *            If true, a TransferSyntaxUID attribute (whose value is
     *            ExplicitVRLittleEndian) will be added to the DICOM object if
     *            it doesn't already exists there.
     */
    private static void enforceExplicitVRLittleEndian(DicomObject obj, boolean addIfNotExists) {
        if (obj.contains(Tag.TransferSyntaxUID)) {
            String transferSyntaxUID = obj.getString(Tag.TransferSyntaxUID);
            if (transferSyntaxUID.equals(TransferSyntax.ExplicitVRBigEndian.uid())
                    || transferSyntaxUID.equals(TransferSyntax.ImplicitVRLittleEndian.uid())) {
                obj.putString(Tag.TransferSyntaxUID, VR.UI,
                        TransferSyntax.ExplicitVRLittleEndian.uid());
            }
        } else if (addIfNotExists) {
            obj.putString(Tag.TransferSyntaxUID, VR.UI, TransferSyntax.ExplicitVRLittleEndian.uid());
        }
    }
}
