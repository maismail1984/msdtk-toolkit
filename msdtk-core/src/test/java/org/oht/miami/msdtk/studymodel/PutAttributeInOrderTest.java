/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdtk.studymodel;

import static org.junit.Assert.assertEquals;

import org.oht.miami.msdtk.store.Store;
import org.oht.miami.msdtk.store.StoreFactory;
import org.oht.miami.msdtk.util.DicomUID;

import org.dcm4che2.data.BasicDicomObject;
import org.dcm4che2.data.DicomElement;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.SimpleDicomElement;
import org.dcm4che2.data.Tag;
import org.dcm4che2.data.VR;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

/**
 * Test the binary search algorithm in putAttributeInOrder method in Series and
 * in Instance
 * 
 * @author Jiefeng Zhai
 */
public class PutAttributeInOrderTest {

    private Study studyNormalized = null;

    private Series series = null;

    private Instance instance = null;

    private Store store = StoreFactory.createStore("testStoreFS.properties");

    @Before
    public void setUp() throws IOException {
        final String STUDY_UID_VALUE = "1.3.6.1.4.1.5962.1.3.10.2.1166562673.14401";
        DicomElement studyUID = new SimpleDicomElement(Tag.StudyInstanceUID, VR.UI, true,
                STUDY_UID_VALUE.getBytes(), null);
        final String SERIES_UID_VALUE = "1.3.6.1.4.1.5962.1.3.10.2.1166562673.14401";
        DicomElement seriesUID = new SimpleDicomElement(Tag.SeriesInstanceUID, VR.UI, true,
                SERIES_UID_VALUE.getBytes(), null);
        final String SOP_UID_VALUE = "1.3.6.1.4.1.5962.1.1.10.2.1.1166562673.14401";
        DicomElement sopUID = new SimpleDicomElement(Tag.SOPInstanceUID, VR.UI, true,
                SOP_UID_VALUE.getBytes(), null);

        DicomObject obj = new BasicDicomObject();
        obj.add(sopUID);
        obj.add(studyUID);
        obj.add(seriesUID);

        studyNormalized = new Study(store, new DicomUID(STUDY_UID_VALUE), true, obj);

        series = studyNormalized.getSeries(SERIES_UID_VALUE);
        instance = studyNormalized.getInstance(SOP_UID_VALUE);
    }

    @After
    public void tearDown() {
        studyNormalized = null;
        series = null;
        instance = null;
    }

    /**
     * Put attribute in order to the series
     */
    @Test
    public void testPutAttributeInOrderSeries() {
        String value01 = "St. Nowhere Hospital";
        DicomElement attr01 = new SimpleDicomElement(Tag.InstitutionName, VR.LO, true,
                value01.getBytes(), null);
        String value02 = "Thomas^Albert";
        DicomElement attr02 = new SimpleDicomElement(Tag.ReferringPhysicianName, VR.PN, true,
                value02.getBytes(), null);
        String value03 = "20120531";
        DicomElement attr03 = new SimpleDicomElement(Tag.StudyDate, VR.DA, true,
                value03.getBytes(), null);
        String value04 = "3700 N Charles St";
        DicomElement attr04 = new SimpleDicomElement(Tag.PerformedLocation, VR.SH, true,
                value04.getBytes(), null);

        assertEquals(series.getNumberOfSeriesAttributes(), 1);
        assertEquals(series.getAttributeAtIndexOf(0).tag(), Tag.SeriesInstanceUID);

        series.putAttributeInOrder(attr01);
        series.putAttributeInOrder(attr02);
        series.putAttributeInOrder(attr03);
        series.putAttributeInOrder(attr04);

        assertEquals(series.getNumberOfSeriesAttributes(), 5);
        assertEquals(series.getAttributeAtIndexOf(0).tag(), Tag.StudyDate);
        assertEquals(series.getAttributeAtIndexOf(1).tag(), Tag.InstitutionName);
        assertEquals(series.getAttributeAtIndexOf(2).tag(), Tag.ReferringPhysicianName);
        assertEquals(series.getAttributeAtIndexOf(3).tag(), Tag.SeriesInstanceUID);
        assertEquals(series.getAttributeAtIndexOf(4).tag(), Tag.PerformedLocation);
    }

    /**
     * Put attribute in order to the instance
     */
    @Test
    public void testPutAttributeInOrderInstance() {
        String value01 = "St. Nowhere Hospital";
        DicomElement attr01 = new SimpleDicomElement(Tag.InstitutionName, VR.LO, true,
                value01.getBytes(), null);
        String value02 = "Thomas^Albert";
        DicomElement attr02 = new SimpleDicomElement(Tag.StationName, VR.SH, true,
                value02.getBytes(), null);
        String value03 = "Johns Hopkins";
        DicomElement attr03 = new SimpleDicomElement(Tag.Manufacturer, VR.LO, true,
                value03.getBytes(), null);
        String value04 = "1.3.6.1.4.8.5962.1.3.10.2.1166562673.14401";
        DicomElement attr04 = new SimpleDicomElement(Tag.SOPClassUID, VR.UI, true,
                value04.getBytes(), null);

        assertEquals(instance.elementCount(), 1);
        assertEquals(instance.getAttributeAtIndexOf(0).tag(), Tag.SOPInstanceUID);

        instance.putAttributeInOrder(attr01);
        instance.putAttributeInOrder(attr02);
        instance.putAttributeInOrder(attr03);
        instance.putAttributeInOrder(attr04);

        assertEquals(instance.elementCount(), 5);
        assertEquals(instance.getAttributeAtIndexOf(0).tag(), Tag.SOPClassUID);
        assertEquals(instance.getAttributeAtIndexOf(1).tag(), Tag.SOPInstanceUID);
        assertEquals(instance.getAttributeAtIndexOf(2).tag(), Tag.Manufacturer);
        assertEquals(instance.getAttributeAtIndexOf(3).tag(), Tag.InstitutionName);
        assertEquals(instance.getAttributeAtIndexOf(4).tag(), Tag.StationName);
    }
}
