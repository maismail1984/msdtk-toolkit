/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdtk.studymodel;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.oht.miami.msdtk.deidentification.DeIdentifyUtil;
import org.oht.miami.msdtk.deidentification.StudyProfileOptions;
import org.oht.miami.msdtk.store.Store;
import org.oht.miami.msdtk.store.StoreFactory;
import org.oht.miami.msdtk.util.DicomUID;
import org.oht.miami.msdtk.util.TestUtils;

import org.dcm4che2.data.BasicDicomObject;
import org.dcm4che2.data.DicomElement;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.SimpleDicomElement;
import org.dcm4che2.data.Tag;
import org.dcm4che2.data.VR;
import org.dcm4che2.io.DicomInputStream;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

/**
 * Tests the De-Identification of patient information
 * 
 * @author Katie Calabro kcalabr1@jhu.edu
 */
public class StudyDeIdentificationTest {

    private Study testStudy;

    private Store store;

    /**
     * Sets up the new study
     */
    @Before
    public void setUp() {
        store = StoreFactory.createStore("testStoreFS.properties");

        DicomElement attribute1 = createDicomElement(Tag.StudyInstanceUID, VR.UI, "1");
        DicomElement attribute2 = createDicomElement(Tag.SeriesInstanceUID, VR.UI, "1");
        DicomElement attribute3 = createDicomElement(Tag.SOPInstanceUID, VR.UI, "1");
        DicomElement attribute4 = createDicomElement(Tag.PatientName, VR.LO, "Joe Smith");
        DicomElement attribute5 = createDicomElement(Tag.PatientSex, VR.LO, "Male");
        DicomElement attribute6 = createDicomElement(Tag.PatientAge, VR.LO, "40");

        DicomObject studyTestDcmObj = new BasicDicomObject();
        studyTestDcmObj.add(attribute1);
        studyTestDcmObj.add(attribute2);
        studyTestDcmObj.add(attribute3);
        studyTestDcmObj.add(attribute4);
        studyTestDcmObj.add(attribute5);
        studyTestDcmObj.add(attribute6);

        testStudy = new Study(store, new DicomUID(), true, studyTestDcmObj);
    }

    @After
    public void tearDown() throws IOException {
        TestUtils.cleanStore(store);
    }

    /**
     * Creates a Dicom Element
     * 
     * @param testTag
     *            The int value of the tag
     * @param id
     *            The VR id
     * @param testValue
     *            The value that will be in the dicom element
     * @return The Dicom element created
     */
    public DicomElement createDicomElement(int testTag, VR id, String testValue) {
        DicomElement testElement = new SimpleDicomElement(testTag, id, true, testValue.getBytes(),
                null);
        return testElement;
    }

    /**
     * Checks the content of the input byte array equals to zero
     * 
     * @param b
     *            The byte array
     */
    public void checkByteArrayContents(byte[] b) {
        for (int i = 0; i < 128; i++)
            assertEquals((int) b[i], 0);
    }

    /**
     * [MSDTK-18] Constructs a Dicom study. Then it de-identifies the study and
     * verifies the patient related attributes have been removed.
     */
    @Test
    public void testBasicProfileDeIdentification() {
        /* makes sure there are values for the patient attributes */
        assertNotNull(testStudy.getValueForAttributeAsString(Tag.PatientName));
        assertNotNull(testStudy.getValueForAttributeAsString(Tag.PatientSex));
        assertNotNull(testStudy.getValueForAttributeAsString(Tag.PatientAge));

        StudyProfileOptions pOptions = new StudyProfileOptions();
        testStudy.deIdentifyStudy(pOptions);

        assertNull(testStudy.getValueForAttributeAsString(Tag.PatientName));
        assertNull(testStudy.getValueForAttributeAsString(Tag.PatientSex));
        assertNull(testStudy.getValueForAttributeAsString(Tag.PatientAge));
    }

    /**
     * [MSDTK-20]Constructs a Dicom study and de-Identifies the study using the
     * basic profile and keep UID option. Then it verifies the instance UID has
     * not changed.
     */
    public void testVerifyInstanceUIDIsKeptWithKeepUIDOption() {
        assertEquals(testStudy.getValueForAttributeAsString(Tag.StudyInstanceUID), "1");

        StudyProfileOptions pOptions = new StudyProfileOptions(true, false, true, false, false,
                false, false, false, false, false);
        testStudy.deIdentifyStudy(pOptions);

        assertEquals(testStudy.getValueForAttributeAsString(Tag.StudyInstanceUID), "1");
    }

    /**
     * [MSDTK-38] Constructs a Dicom study. Then it de-identifies the study
     * using the basic profile and verifies the instance UID is protected
     * (updated).
     */
    @Test
    public void testInstanceUIDIsProtectedAfterDeIdentification() {
        StudyProfileOptions pOptions = new StudyProfileOptions();
        Instance testInstance = testStudy.getInstance("1");

        assertTrue(testInstance.getValueForAttributeAsString(Tag.SOPInstanceUID).equals("1"));
        testStudy.deIdentifyStudy(pOptions);
        assertFalse(testInstance.getValueForAttributeAsString(Tag.SOPInstanceUID).equals("1"));
    }

    /**
     * [MSDTK-21] Creates a sequence element to be de-identified. Then it adds
     * items to the sequence element. Then it applies the clean action on the
     * sequence element and ensures the basic profile actions rules are applied
     * recursively on the child items.
     */
    @Test
    public void testCleanActionforSequenceItem() {

        DicomElement attribute1 = createDicomElement(Tag.StudyInstanceUID, VR.UI, "1");
        DicomElement attribute2 = createDicomElement(Tag.SeriesInstanceUID, VR.UI, "1");
        DicomElement attribute3 = createDicomElement(Tag.SOPInstanceUID, VR.UI, "1");
        DicomElement attribute4 = createDicomElement(Tag.PatientName, VR.LO, "Joe Smith");
        DicomElement attribute5 = createDicomElement(Tag.PatientSex, VR.LO, "Male");
        DicomElement attribute6 = createDicomElement(Tag.PatientAge, VR.LO, "40");
        DicomElement attribute8 = createDicomElement(Tag.AcquisitionComments, VR.LT, "comments");
        DicomElement attribute9 = createDicomElement(Tag.AccessionNumber, VR.SH, "1");
        DicomElement attribute10 = createDicomElement(Tag.ContributionDescription, VR.ST,
                "discription");

        DicomObject testDcmObj = new BasicDicomObject();
        testDcmObj.add(attribute1);
        testDcmObj.add(attribute2);
        testDcmObj.add(attribute3);
        testDcmObj.add(attribute4);
        testDcmObj.add(attribute5);
        testDcmObj.add(attribute6);

        DicomElement sequenceElement = testDcmObj
                .putSequence(Tag.PerformedStationGeographicLocationCodeSequence);

        DicomObject testDcmObj2 = new BasicDicomObject();
        testDcmObj2.add(attribute8);
        testDcmObj2.add(attribute9);
        testDcmObj2.add(attribute10);

        sequenceElement.addDicomObject(testDcmObj2);

        testDcmObj.add(sequenceElement);

        Study testStudy2 = new Study(store, new DicomUID(), true, testDcmObj);
        assertNotNull(testStudy2);
        StudyProfileOptions pOptions = new StudyProfileOptions(true, true, true, true, false,
                false, false, false, false, false);
        testStudy2.deIdentifyStudy(pOptions);

        assertNull(testStudy2.getAttribute(Tag.PerformedStationGeographicLocationCodeSequence)
                .getDicomObject().getString(Tag.AcquisitionComments));
        assertNull(testStudy2.getAttribute(Tag.PerformedStationGeographicLocationCodeSequence)
                .getDicomObject().getString(Tag.AccessionNumber));
        assertNull(testStudy2.getAttribute(Tag.PerformedStationGeographicLocationCodeSequence)
                .getDicomObject().getString(Tag.ContributionDescription));
    }

    /**
     * [MSDTK-19] Constructs a Dicom study, then it de-identifies the study and
     * verifies that the attribute "Patient Identity Removed" (0012,0062) exists
     * in the study model with value of YES, and one or more codes from PS 3.16
     * CID 7050 (Dicom standard, page 607) De-identification Method
     * corresponding to the profile and options is added to De-identification
     * Method Code Sequence (0012,0064)
     */
    @Test
    public void testVerifyTheDeIdentificationInfoIsAdded() {
        StudyProfileOptions pOptions = new StudyProfileOptions();
        assertNotNull(testStudy);
        testStudy.deIdentifyStudy(pOptions);

        assertEquals(testStudy.getValueForAttributeAsString(Tag.PatientIdentityRemoved), "YES");
        assertEquals(
                testStudy.getValueForAttributeAsString(Tag.DeidentificationMethodCodeSequence),
                DeIdentifyUtil.BasicApplicationConfidentialityProfile);
    }

    /**
     * [MSDTK-37]Constructs a Dicom study. Then it de-identifies the study and
     * writes it to a Dicom file. It then ensures the written file does not have
     * the 128 preamble bytes.
     * 
     * @throws IOException
     */
    @Test
    public void testDeIdentificationIntegration() throws IOException {
        final String TEST_STUDY_NAME = "20phase";

        /* get the path of the file */
        final String CURRENTDIR = "target/test-out/StudyTest";
        File outputDir = new File(CURRENTDIR);
        outputDir.mkdirs();

        /* reads the study */
        Study studyNormalized = TestUtils.readStudyFromResource(store, TEST_STUDY_NAME);

        /* de-identifies the study */
        StudyProfileOptions pOptions = new StudyProfileOptions();
        studyNormalized.deIdentifyStudy(pOptions);

        assertNull(studyNormalized.getValueForAttributeAsString(Tag.PatientSex));
        assertNull(studyNormalized.getValueForAttributeAsString(Tag.PatientName));
        assertNull(studyNormalized.getValueForAttributeAsString(Tag.PatientAge));

        /* writes a study to a file */
        TestUtils.writeSingleFrame(studyNormalized, outputDir, TEST_STUDY_NAME, false);
        /*
         * Iterates through all the files that were just created to ensure that
         * the preamble is 128 bytes
         */
        File[] dir = new File(CURRENTDIR + "/20phase_SFD/").listFiles();
        for (File f : dir) {
            if (!f.isHidden()) {
                File DicomFile = new File(CURRENTDIR + "/20phase_SFD/" + f.getName());
                DicomInputStream din = new DicomInputStream(DicomFile);
                try {
                    assertEquals(din.getPreamble().length, 128);
                    checkByteArrayContents(din.getPreamble());
                } finally {
                    din.close();
                }
            }
        }
    }

    /**
     * [MSDTK-105] Calls the function testReportOfSeriesInStudy in
     * StudyReport.java to write a report to a study to a .txt file
     * 
     * @throws IOException
     */
    // TODO: This test will be converted into a utility program. May be added to
    // IntegrationMain.java
    @Test
    public void testReportOfSeriesInStudy() throws IOException {
        String report = "target/test-out/StudyReport.txt";
        StudyReport.writeSeriesReport(store, "20phase", report);
    }
}
