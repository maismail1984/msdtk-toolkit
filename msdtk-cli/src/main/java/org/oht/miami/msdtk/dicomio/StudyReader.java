/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdtk.dicomio;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.dcm4che2.io.DicomInputStream;
import org.oht.miami.msdtk.store.Store;
import org.oht.miami.msdtk.studymodel.Study;

/**
 * Reads a sequence of DICOM streams and builds one or more MS-DICOM study
 * models.
 * 
 * @author Raphael Yu Ning
 */
public abstract class StudyReader {

    protected final Store store;

    protected abstract boolean checkInput();

    protected abstract InputStream nextInputStream() throws IOException;

    protected StudyReader(Store store) {

        if (store == null) {
            throw new IllegalArgumentException("Store must not be null");
        }
        this.store = store;
    }

    public List<Study> readStudies() throws IOException {

        if (!checkInput()) {
            throw new IllegalStateException("Input has not been specified");
        }

        final List<Study> studies = new ArrayList<Study>();

        // Set timeout to infinity to ensure no studies expire automatically
        ActiveStudyTable activeStudyTable = new ActiveStudyTable(store, Long.MAX_VALUE,
                TimeUnit.NANOSECONDS, new ActiveStudyTable.StudyTimeoutHandler() {

                    @Override
                    public void onStudyTimeout(Study study) throws Exception {

                        // No need to synchronize because study timeout handlers
                        // are executed synchronously by default
                        studies.add(study);
                    }
                });

        StudyParser parser = new StudyParser(activeStudyTable);
        InputStream in = null;
        while ((in = nextInputStream()) != null) {
            DicomInputStream dicomIn = new DicomInputStream(new BufferedInputStream(in));
            try {
                parser.parse(dicomIn);
            } finally {
                dicomIn.close();
            }
        }

        // Manually time out all studies
        activeStudyTable.clear();
        return studies;
    }

    public Study readStudy() throws IOException {

        if (!checkInput()) {
            throw new IllegalStateException("Input has not been specified");
        }

        StudyParser parser = new StudyParser(store);
        Study study = null;
        InputStream in = null;
        while ((in = nextInputStream()) != null) {
            DicomInputStream dicomIn = new DicomInputStream(new BufferedInputStream(in));
            try {
                study = parser.parse(dicomIn, study);
            } finally {
                dicomIn.close();
            }
        }

        study.getBulkDataSet().close();
        return study;
    }
}
