/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdtk.util;

import java.io.IOException;
import java.io.OutputStream;

/**
 * Class to encapsulate reading and writing to a stream with MIME Separators
 * 
 * @author rstonebr
 */
public class MultiPartMimeStreamConverter {
    /**
     * Content header which describes the body of the MIME part
     */
    private static final String MIME_PART_HEADER = "\n"
            + "Content-Type: application/octet-stream\n" + "Content-Transfer-Encoding: binary\n\n";
    /**
     * Multi-part MIME boundary which is placed between the parts
     */
    private static final String MIME_PART_BOUNDARY = "\n" + "--MultiPartMIMESeparatorMSDicom";
    /**
     * MIME_PART_HEADER encoded into bytes
     */
    private static final byte[] MIME_PART_HEADER_BYTES = MIME_PART_HEADER.getBytes();
    /**
     * MIME_PART_BOUNDARY encoded into bytes
     */
    private static final byte[] MIME_PART_BOUNDARY_BYTES = MIME_PART_BOUNDARY.getBytes();
    /**
     * Byte length of MIME_PART_HEADER
     */
    private static final int MIME_PART_HEADER_LENGTH = MIME_PART_HEADER_BYTES.length;
    /**
     * Byte length of MIME_PART_BOUNDARY
     */
    private static final int MIME_PART_BOUNDARY_LENGTH = MIME_PART_BOUNDARY_BYTES.length;

    /**
     * Writes the given binary data to the given input output stream while
     * prefixing a MIME separator to the data
     * 
     * @param out
     *            the output stream.
     * @param offset
     *            the stream position where the next byte will be written
     * @param data
     *            the binary data to write
     * @return the actual offset of the data (i.e. the stream position of the
     *         first byte of the data)
     * @throws IOException
     */
    public static int write(final OutputStream out, int offset, byte[] data) throws IOException {
        out.write(MIME_PART_BOUNDARY_BYTES);
        out.write(MIME_PART_HEADER_BYTES);
        out.write(data);
        return offset + MIME_PART_BOUNDARY_LENGTH + MIME_PART_HEADER_LENGTH;
    }
}
