/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdtk.studymodel;

import static org.junit.Assert.assertTrue;

import org.oht.miami.msdtk.store.Store;
import org.oht.miami.msdtk.store.StoreFactory;
import org.oht.miami.msdtk.testing.TestDataLocator;
import org.oht.miami.msdtk.util.TestUtils;

import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.SequenceDicomElement;
import org.dcm4che2.data.Tag;
import org.dcm4che2.io.DicomInputStream;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Writes a report of a study
 * 
 * @author Katie Calabro kcalabr1@jhu.edu
 */
public class StudyReport {

    private Store store;

    @Before
    public void setUp() {
        store = StoreFactory.createStore("testStoreFS.properties");
    }

    @After
    public void tearDown() throws IOException {
        TestUtils.cleanStore(store);
    }

    /**
     * [MSDTK-105] Creates a function that reports the series within a study. It
     * prints the seriesInstanceUID, that series Instance's and there UID's, and
     * the path to the file for the Instance. All of this information is written
     * to a .txt file
     * 
     * @param store
     *            MSD-Store used when reading the study
     * @param studyName
     *            Name of the study
     * @param reportPath
     *            String path of where to write the .txt report
     * @throws IOException
     */
    public static void writeSeriesReport(Store store, String studyName, String reportPath)
            throws IOException {
        /* reads the study */
        Study readStudy = TestUtils.readStudyFromResource(store, studyName);

        int numSeries = 1;
        int numInstance = 1;
        String fileName = "";

        File reportFile = new File(reportPath);
        if (!reportFile.getParentFile().exists()) {
            throw new IllegalArgumentException("No such file exists");
        }
        FileWriter fWriter = new FileWriter(reportFile);
        BufferedWriter writer = new BufferedWriter(fWriter);

        TestDataLocator locator = new TestDataLocator();
        List<String> studyResources = locator.locateStudy(studyName);

        writer.write("StudyInstanceUID:  "
                + readStudy.getValueForAttributeAsString(Tag.StudyInstanceUID) + "\n\n");

        for (Iterator<Series> iter = readStudy.seriesIterator(); iter.hasNext();) {
            Series testSeries = iter.next();
            writer.write("Series #" + numSeries + "\nUID:  " + testSeries.getSeriesInstanceUID()
                    + "\n");
            numSeries++;
            for (Iterator<Instance> instanceIter = testSeries.instanceIterator(); instanceIter
                    .hasNext();) {
                Instance testInstance = instanceIter.next();
                for (String studyResource : studyResources) {
                    String resourceName = studyResource.substring("classpath:".length());
                    InputStream in = Thread.currentThread().getContextClassLoader()
                            .getResourceAsStream(resourceName);
                    DicomInputStream din = new DicomInputStream(in);
                    DicomObject dcmObj = null;
                    try {
                        dcmObj = din.readDicomObject();
                    } finally {
                        din.close();
                    }
                    if (testInstance.getSOPInstanceUID().equals(
                            dcmObj.getString(Tag.SOPInstanceUID))) {
                        fileName = resourceName;
                        break;
                    }
                }

                writer.write("\nInstance #" + numInstance + "\nUID:  "
                        + testInstance.getSOPInstanceUID() + "\nFile Location:  " + fileName + "\n");
                numInstance++;
            }
            writer.write("---------------------------------\n");
        }
        writer.close();
    }

    /**
     * [MSDTK-31] Compares the size of metadata for a study stored in different
     * formats (MSD, SFD) after subtracting the binary data size. It is then
     * Repeated for a study in MFD.
     * 
     * @throws IOException
     */
    // TODO convert this test to a utility function.
    @Test
    @Ignore
    public void testSizeOfDataElements() throws IOException {

        final String TEST_STUDY_NAME = "20phase";
        long fileSize_SFD = 0;
        long fileSize_MFD = 0;
        long fileSize_MSD = 0;
        int pixelDataSize_SFD = 0;
        int pixelDataSize_MFD = 0;
        int pixelDataSize_MSD = 0;
        long metaDataSize_SFD = 0;
        long metaDataSize_MSD = 0;
        long metaDataSize_MFD = 0;
        File[] dir = null;
        String path = "";
        File dicomFile = null;

        /* get the path of the file */
        List<String> studyResources = new ArrayList<String>();
        TestDataLocator studyLocator = new TestDataLocator();
        studyResources = studyLocator.locateStudy("20phase");

        /* gets the sizes of the pixel and meta data */
        for (String studyResource : studyResources) {
            String resourceName = studyResource.substring("classpath:".length());
            URL url = Thread.currentThread().getContextClassLoader().getResource(resourceName);
            dicomFile = new File(url.getPath());
            fileSize_SFD += dicomFile.length();
            InputStream is = Thread.currentThread().getContextClassLoader()
                    .getResourceAsStream(resourceName);
            DicomInputStream din = new DicomInputStream(is);
            DicomObject DicomObj = din.readDicomObject();
            din.close();
            pixelDataSize_SFD += DicomObj.get(Tag.PixelData).length();
            metaDataSize_SFD += fileSize_SFD - pixelDataSize_SFD;
        }

        // ***MFD STUDY***/
        /* get the path of the file */
        final String CURRENTDIR_MFD = "target/test-out/StudyTest";
        File outputDirMFD = new File(CURRENTDIR_MFD);
        outputDirMFD.mkdirs();

        /* reads the study */
        Study studyMFD = TestUtils.readStudyFromResource(store, TEST_STUDY_NAME);

        /* writes a study to a file */
        TestUtils.writeMultiFrame(studyMFD, outputDirMFD, TEST_STUDY_NAME);

        /* gets the sizes of the pixel and meta data */
        path = CURRENTDIR_MFD + "/20phase_MFD/";
        dir = new File(path).listFiles();
        for (File f : dir) {
            if (!f.isHidden()) {
                dicomFile = new File(path + f.getName());
                fileSize_MFD += dicomFile.length();
                DicomInputStream din = new DicomInputStream(dicomFile);
                DicomObject DicomObj = din.readDicomObject();
                din.close();
                pixelDataSize_MFD += DicomObj.get(Tag.PixelData).length();
                metaDataSize_MFD += fileSize_MFD - pixelDataSize_MFD;
            }
        }

        // ***MSD STUDY***/
        /* get the path of the file */
        final String CURRENTDIR_MSD = "target/test-out/StudyTest";
        File outputDirMSD = new File(CURRENTDIR_MFD);
        outputDirMSD.mkdirs();

        /* reads the study */
        Study studyMSD = TestUtils.readStudyFromResource(store, TEST_STUDY_NAME);

        /* writes a study to a file */
        TestUtils.writeMultiSeries(studyMSD, outputDirMSD, TEST_STUDY_NAME);

        /* gets the sizes of the pixel and meta data */
        path = CURRENTDIR_MSD + "/20phase_MSD/";
        dir = new File(path).listFiles();
        for (File f : dir) {
            if (!f.isHidden()) {
                dicomFile = new File(path + f.getName());
                fileSize_MSD += dicomFile.length();
                DicomInputStream din = new DicomInputStream(dicomFile);
                DicomObject DicomObj = din.readDicomObject();
                din.close();
                SequenceDicomElement test = (SequenceDicomElement) DicomObj
                        .get(Tag.PerSeriesFunctionalGroupsSequence);
                for (int i = 0; i < test.countItems(); i++) {
                    DicomObject tmpDcmObj = test.getDicomObject(i);
                    pixelDataSize_MSD += tmpDcmObj.get(Tag.PixelData).length();
                }
                metaDataSize_MSD += fileSize_MSD - pixelDataSize_MSD;
            }
        }

        System.out.println("pixelDataSize_SFD:  " + pixelDataSize_SFD);
        System.out.println("pixelDataSize_MFD:  " + pixelDataSize_MFD);
        System.out.println("pixelDataSize_MSD:  " + pixelDataSize_MSD);
        System.out.println();
        System.out.println("metaDataSize_SFD:  " + metaDataSize_SFD);
        System.out.println("metaDataSize_MFD:  " + metaDataSize_MFD);
        System.out.println("metaDataSize_MSD:  " + metaDataSize_MSD);

        assertTrue(pixelDataSize_SFD == pixelDataSize_MFD);
        assertTrue(pixelDataSize_MFD == pixelDataSize_MSD);
        assertTrue(metaDataSize_SFD > metaDataSize_MFD);
        assertTrue(metaDataSize_MFD > metaDataSize_MSD);
        assertTrue(metaDataSize_SFD > metaDataSize_MSD);
    }
}
