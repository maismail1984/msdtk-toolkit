To remove patient identifying information from an input study use the following command:

java -jar msdtk-cli-1.0.jar  input  output clean normalize

Where input is a directory that has all the dicom instances associated with one study. output is empty (or non-existing) folder that has the generated de-identified study.
If you want to run the utility for multiple studies please do a separate call for each study. You can wrap the calls in a simple shell script
