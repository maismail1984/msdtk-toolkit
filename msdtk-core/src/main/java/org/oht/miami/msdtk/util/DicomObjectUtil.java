/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdtk.util;

import org.dcm4che2.data.DicomElement;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;

import java.util.Iterator;

/**
 * Class to provide utilities for DicomObject class.
 * @author Mahmoud Ismail(maismail@cs.jhu.edu)
 */
public class DicomObjectUtil {

    /**
     * Utility function to remove specific file meta information attributes from
     * the input DICOM object
     * 
     * @param dcmObj
     */
    public static void removeFileMetaInformation(DicomObject dcmObj) {
        Iterator<DicomElement> attributeIter = dcmObj.fileMetaInfoIterator();
        while (attributeIter.hasNext()) {
            DicomElement currentElement = attributeIter.next();
            if (currentElement.tag() != Tag.TransferSyntaxUID) {
                dcmObj.remove(currentElement.tag());
            }
        }
    }

}
