/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdtk.studymodel;

import org.dcm4che2.data.AbstractDicomElement;
import org.dcm4che2.data.DateRange;
import org.dcm4che2.data.DicomElement;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.SpecificCharacterSet;
import org.dcm4che2.data.VR;

import java.util.Date;
import java.util.regex.Pattern;

/**
 * A special DicomElement whose value is a BulkDataReference.
 * 
 * @author Raphael Yu Ning
 */
public class BulkDataDicomElement extends AbstractDicomElement {

    private static final long serialVersionUID = 3875098497475301277L;

    private BulkDataReference bdr;

    private volatile Object cachedValue;

    public BulkDataDicomElement(int tag, boolean bigEndian, BulkDataReference bdr) {

        super(tag, VR.BD, bigEndian);
        if (bdr == null) {
            throw new IllegalArgumentException("Bulk Data Reference must not be null");
        }
        this.bdr = bdr;
        cachedValue = null;
    }

    @Override
    public int vm(SpecificCharacterSet cs) {

        return 1;
    }

    @Override
    public int length() {

        return BulkDataReference.BDR_VALUE_LENGTH;
    }

    @Override
    public boolean isEmpty() {

        return bdr.getLength() == 0;
    }

    @Override
    public boolean hasItems() {

        return false;
    }

    @Override
    public int countItems() {

        return 0;
    }

    @Override
    public byte[] getBytes() {

        return bdr.toBytes(this.bigEndian);
    }

    @Override
    public boolean hasDicomObjects() {

        return false;
    }

    @Override
    public boolean hasFragments() {

        return false;
    }

    @Override
    public DicomObject getDicomObject() {

        throw new UnsupportedOperationException();
    }

    @Override
    public DicomObject getDicomObject(int index) {

        throw new UnsupportedOperationException();
    }

    @Override
    public DicomObject removeDicomObject(int index) {

        throw new UnsupportedOperationException();
    }

    @Override
    public boolean removeDicomObject(DicomObject item) {

        throw new UnsupportedOperationException();
    }

    @Override
    public DicomObject addDicomObject(DicomObject item) {

        throw new UnsupportedOperationException();
    }

    @Override
    public DicomObject addDicomObject(int index, DicomObject item) {

        throw new UnsupportedOperationException();
    }

    @Override
    public DicomObject setDicomObject(int index, DicomObject item) {

        throw new UnsupportedOperationException();
    }

    @Override
    public byte[] getFragment(int index) {

        throw new UnsupportedOperationException();
    }

    @Override
    public byte[] removeFragment(int index) {

        throw new UnsupportedOperationException();
    }

    @Override
    public boolean removeFragment(byte[] b) {

        throw new UnsupportedOperationException();
    }

    @Override
    public byte[] addFragment(byte[] b) {

        throw new UnsupportedOperationException();
    }

    @Override
    public byte[] addFragment(int index, byte[] b) {

        throw new UnsupportedOperationException();
    }

    @Override
    public byte[] setFragment(int index, byte[] b) {

        throw new UnsupportedOperationException();
    }

    @Override
    public short[] getShorts(boolean cache) {

        if (cache) {
            Object tmp = cachedValue;
            if (tmp instanceof short[]) {
                return (short[]) tmp;
            }
        }
        short[] value = vr.toShorts(getBytes(), this.bigEndian);
        if (cache) {
            cachedValue = value;
        }
        return value;
    }

    @Override
    public int getInt(boolean cache) {

        if (cache) {
            Object tmp = cachedValue;
            if (tmp instanceof Integer) {
                return ((Integer) tmp).intValue();
            }
        }
        int value = vr.toInt(getBytes(), this.bigEndian);
        if (cache) {
            cachedValue = Integer.valueOf(value);
        }
        return value;
    }

    @Override
    public int[] getInts(boolean cache) {

        if (cache) {
            Object tmp = cachedValue;
            if (tmp instanceof int[]) {
                return (int[]) tmp;
            }
        }
        int[] value = vr.toInts(getBytes(), this.bigEndian);
        if (cache) {
            cachedValue = value;
        }
        return value;
    }

    @Override
    public float getFloat(boolean cache) {

        if (cache) {
            Object tmp = cachedValue;
            if (tmp instanceof Float) {
                return ((Float) tmp).floatValue();
            }
        }
        float value = vr.toFloat(getBytes(), this.bigEndian);
        if (cache) {
            cachedValue = new Float(value);
        }
        return value;
    }

    @Override
    public float[] getFloats(boolean cache) {

        if (cache) {
            Object tmp = cachedValue;
            if (tmp instanceof float[]) {
                return (float[]) tmp;
            }
        }
        float[] value = vr.toFloats(getBytes(), this.bigEndian);
        if (cache) {
            cachedValue = value;
        }
        return value;
    }

    @Override
    public double getDouble(boolean cache) {

        if (cache) {
            Object tmp = cachedValue;
            if (tmp instanceof Double) {
                return ((Double) tmp).doubleValue();
            }
        }
        double value = vr.toDouble(getBytes(), this.bigEndian);
        if (cache) {
            cachedValue = new Double(value);
        }
        return value;
    }

    @Override
    public double[] getDoubles(boolean cache) {

        if (cache) {
            Object tmp = cachedValue;
            if (tmp instanceof double[]) {
                return (double[]) tmp;
            }
        }
        double[] value = vr.toDoubles(getBytes(), this.bigEndian);
        if (cache) {
            cachedValue = value;
        }
        return value;
    }

    @Override
    public String getString(SpecificCharacterSet cs, boolean cache) {

        if (cache) {
            Object tmp = cachedValue;
            if (tmp instanceof String) {
                return (String) tmp;
            }
        }
        String value = vr.toString(getBytes(), this.bigEndian, cs);
        if (cache) {
            cachedValue = value;
        }
        return value;
    }

    @Override
    public String[] getStrings(SpecificCharacterSet cs, boolean cache) {

        if (cache) {
            Object tmp = cachedValue;
            if (tmp instanceof String[]) {
                return (String[]) tmp;
            }
        }
        String[] value = vr.toStrings(getBytes(), this.bigEndian, cs);
        if (cache) {
            cachedValue = value;
        }
        return value;
    }

    @Override
    public Date getDate(boolean cache) {

        if (cache) {
            Object tmp = cachedValue;
            if (tmp instanceof Date) {
                return (Date) tmp;
            }
        }
        Date value = vr.toDate(getBytes());
        if (cache) {
            cachedValue = value;
        }
        return value;
    }

    @Override
    public Date[] getDates(boolean cache) {

        if (cache) {
            Object tmp = cachedValue;
            if (tmp instanceof Date[]) {
                return (Date[]) tmp;
            }
        }
        Date[] value = vr.toDates(getBytes());
        if (cache) {
            cachedValue = value;
        }
        return value;
    }

    @Override
    public DateRange getDateRange(boolean cache) {

        if (cache) {
            Object tmp = cachedValue;
            if (tmp instanceof DateRange) {
                return (DateRange) tmp;
            }
        }
        DateRange value = vr.toDateRange(getBytes());
        if (cache) {
            cachedValue = value;
        }
        return value;
    }

    @Override
    public Pattern getPattern(SpecificCharacterSet cs, boolean ignoreCase, boolean cache) {

        if (cache) {
            Object tmp = cachedValue;
            if (tmp instanceof Pattern
                    && ((Pattern) tmp).flags() == (ignoreCase ? (Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE)
                            : 0)) {
                return (Pattern) tmp;
            }
        }
        Pattern value = vr.toPattern(getBytes(), this.bigEndian, cs, ignoreCase);
        if (cache) {
            cachedValue = value;
        }
        return value;
    }

    @Override
    public DicomElement share() {

        throw new UnsupportedOperationException();
    }

    @Override
    public DicomElement filterItems(DicomObject filter) {

        throw new UnsupportedOperationException();
    }

    @Override
    public String getValueAsString(SpecificCharacterSet cs, int truncate) {

        String bdrString = bdr.toString();
        int length = bdrString.length();
        if (length > truncate) {
            return bdrString.substring(0, length - 3) + "...";
        } else {
            return bdrString;
        }
    }

    @Override
    protected void appendValue(StringBuffer sb, int maxValLen) {

        sb.append(getValueAsString(null, maxValLen));
    }

    @Override
    protected void toggleEndian() {

        bdr.toggleEndian();
    }

    public byte[] getBulkDataValueAsBytes() {

        return bdr.getValue();
    }

    public int getBulkDataValueLength() {

        return (bdr.getLength() + 1) & ~1;
    }

    public BulkDataReference getBulkDataReference() {

        return bdr;
    }

    public VR getBulkDataVR() {

        return bdr.getVR();
    }

    @Override
    public void setValue(byte[] value) {

        throw new UnsupportedOperationException();
    }

    @Override
    public void setValue(Date value) {

        throw new UnsupportedOperationException();
    }

    @Override
    public void setValue(int value) {

        throw new UnsupportedOperationException();
    }

    @Override
    public void setValue(short value) {

        throw new UnsupportedOperationException();
    }

    @Override
    public void setValue(float value) {

        throw new UnsupportedOperationException();
    }

    @Override
    public void setValue(String value) {

        throw new UnsupportedOperationException();
    }

    @Override
    public void setValue(String value, SpecificCharacterSet cs) {

        throw new UnsupportedOperationException();
    }
}
