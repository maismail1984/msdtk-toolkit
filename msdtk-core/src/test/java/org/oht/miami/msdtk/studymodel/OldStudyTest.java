/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdtk.studymodel;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import org.oht.miami.msdtk.store.Store;
import org.oht.miami.msdtk.store.StoreFactory;
import org.oht.miami.msdtk.util.TestUtils;

import org.dcm4che2.data.DicomElement;
import org.dcm4che2.data.SimpleDicomElement;
import org.dcm4che2.data.Tag;
import org.dcm4che2.data.VR;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

/**
 * Tests the MS-DICOM Study data model functionality and accuracy.
 * 
 * @author Sam Hu
 * @author Raphael Yu Ning
 */
public class OldStudyTest {

    private Study studyNormalized = null;
    private Store store;
    private static final String TEST_STUDY_NAME = "MINT10";

    @Before
    public void setUp() throws IOException {
        store = StoreFactory.createStore("testStoreFS.properties");
        studyNormalized = TestUtils.readStudyFromResource(store, TEST_STUDY_NAME);
    }

    @After
    public void tearDown() throws IOException {
        TestUtils.cleanStore(store);
    }

    /**
     * Tests for patient name accuracy when creating the data model.
     */
    @Test
    public void testGetPatientName() {

        testGetPatientName(studyNormalized);
    }

    private static void testGetPatientName(Study study) {

        assertEquals("MINT10^MINT10", study.getPatientName());
    }

    /**
     * Compares the expected study IUID with what was loaded into the data
     * model.
     */
    @Test
    public void testGetStudyInstanceUid() {

        testGetStudyInstanceUid(studyNormalized);
    }

    private static void testGetStudyInstanceUid(Study study) {

        assertEquals("2.16.840.1.114255.393386351.1568457295.34445.4",
                study.getStudyInstanceUIDAsString());
    }

    /**
     * Tests putting attributes into a study. <br/>
     * Should throw an exception if an attribute with the same tag already
     * exists in the study.
     */
    @Test
    public void testPutAttribute() {

        testPutAttribute(studyNormalized);
    }

    private static void testPutAttribute(Study study) {

        final int TEST_TAG = 0xffffffff;
        final String TEST_VALUE = "test attribute";
        final String TEST_NEW_VALUE = "another test attribute";

        study.putAttribute(new SimpleDicomElement(TEST_TAG, VR.UN, true, TEST_VALUE.getBytes(),
                null));
        DicomElement attr = study.getAttribute(TEST_TAG);
        assertEquals(TEST_VALUE, new String(attr.getBytes()));

        try {
            study.putAttribute(new SimpleDicomElement(TEST_TAG, VR.AE, false, TEST_NEW_VALUE
                    .getBytes(), null));
            fail("Missing expected exception");
        } catch (IllegalArgumentException e) {
            DicomElement newAttr = study.getAttribute(TEST_TAG);
            assertEquals(TEST_VALUE, new String(newAttr.getBytes()));
        }
    }

    /**
     * Tests getting the value of an attribute in the study.
     */
    @Test
    public void testGetValue() {

        testGetValue(studyNormalized);
    }

    private static void testGetValue(Study study) {

        final int TEST_TAG = 0xffffffff;
        final String TEST_VALUE = "test attribute";

        assertNull(study.getValueForAttributeAsString(TEST_TAG));
        assertNull(study.getValueForAttribute(TEST_TAG));

        study.putAttribute(new SimpleDicomElement(TEST_TAG, VR.UN, true, TEST_VALUE.getBytes(),
                null));
        assertEquals(TEST_VALUE, study.getValueForAttributeAsString(TEST_TAG));
        assertArrayEquals(TEST_VALUE.getBytes(), study.getValueForAttribute(TEST_TAG));
    }

    /**
     * Tests removing an attribute from the study.
     */
    @Test
    public void testRemoveAttribute() {

        testRemoveAttribute(studyNormalized);
    }

    private static void testRemoveAttribute(Study study) {

        final int TEST_TAG = 0xffffffff;
        final String TEST_VALUE = "test attribute";

        study.putAttribute(new SimpleDicomElement(TEST_TAG, VR.UN, true, TEST_VALUE.getBytes(),
                null));
        assertNotNull(study.getAttribute(TEST_TAG));

        study.removeAttribute(TEST_TAG);
        assertNull(study.getAttribute(TEST_TAG));
    }

    /**
     * Compares the expected number of series with what the data model has.
     */
    @Test
    public void testGetSeriesCount() {

        testGetSeriesCount(studyNormalized);
    }

    private static void testGetSeriesCount(Study study) {

        assertEquals(2, study.seriesCount());
    }

    /**
     * Tests adding a series to the study.
     */
    @Test
    public void testAddSeries() {

        testAddSeries(studyNormalized);
    }

    private static void testAddSeries(Study study) {

        final String TEST_SERIES_IUID = "test series";

        Series series = new Series(study);
        series.putAttribute(new SimpleDicomElement(Tag.SeriesInstanceUID, VR.UI, false,
                TEST_SERIES_IUID.getBytes(), null));

        int startingSeriesCount = study.seriesCount();
        study.addSeries(series);

        assertEquals(startingSeriesCount + 1, study.seriesCount());
        assertNotNull(study.getSeries(TEST_SERIES_IUID));
    }

    /**
     * Tests getting a specific series from the study.
     */
    @Test
    public void testGetSeries() {

        testGetSeries(studyNormalized);
    }

    private static void testGetSeries(Study study) {

        assertNotNull(study.getSeries("2.16.840.1.114255.393386351.1568457295.5012.1"));
        assertNull(study.getSeries("this series does not exist"));
    }

    /**
     * Tests removing a specific series from the study.
     */
    @Test
    public void testRemoveSeries() {

        testRemoveSeries(studyNormalized);
    }

    private static void testRemoveSeries(Study study) {

        final String TEST_SERIES_IUID = "2.16.840.1.114255.393386351.1568457295.5012.1";

        study.removeSeries(TEST_SERIES_IUID);
        assertNull(study.getSeries(TEST_SERIES_IUID));

        study.removeSeries("this series does not exist");
    }

    /**
     * Tests getting a specific instance from the study.
     */
    @Test
    public void testGetInstance() {

        testGetInstance(studyNormalized);
    }

    private static void testGetInstance(Study study) {

        final String TEST_SOP_IUID = "2.16.840.1.114255.393386351.1568457295.17895.5";

        assertNotNull(study.getInstance(TEST_SOP_IUID));
        assertNull(study.getInstance("this instance does not exist"));
    }

    /**
     * Compares expected elements in the DICOM file with those loaded into the
     * Study model.
     */
    @Test
    public void testStudyAttributes() {

        testStudyAttributes(studyNormalized);
    }

    private static void testStudyAttributes(Study study) {

        DicomElement attr = null;

        attr = study.getAttribute(Tag.StudyDate);
        // FIXME: SpecificCharacterSet should not always be null
        assertEquals("19900808", attr.getValueAsString(null, 0));
        assertEquals("DA", attr.vr().toString());

        attr = study.getAttribute(Tag.StudyTime);
        assertEquals("072843.0000", attr.getValueAsString(null, 0));
        assertEquals("TM", attr.vr().toString());

        attr = study.getAttribute(Tag.AccessionNumber);
        assertEquals("TEST", attr.getValueAsString(null, 0));
        assertEquals("SH", attr.vr().toString());

        attr = study.getAttribute(Tag.ReferringPhysicianName);
        assertEquals("TEST^TEST", attr.getValueAsString(null, 0));
        assertEquals("PN", attr.vr().toString());

        attr = study.getAttribute(Tag.StudyDescription);
        assertEquals("CHEST PA AND LATERAL", attr.getValueAsString(null, 0));
        assertEquals("LO", attr.vr().toString());

        attr = study.getAttribute(Tag.ProcedureCodeSequence);
        assertEquals("SQ", attr.vr().toString());

        attr = study.getAttribute(Tag.PhysiciansOfRecord);
        assertEquals("JOHNSON^KHALIAH AESHA^^^", attr.getValueAsString(null, 0));
        assertEquals("PN", attr.vr().toString());

        attr = study.getAttribute(Tag.NameOfPhysiciansReadingStudy);
        assertEquals("PN", attr.vr().toString());

        attr = study.getAttribute(Tag.AdmittingDiagnosesDescription);
        assertEquals("LO", attr.vr().toString());

        attr = study.getAttribute(Tag.ReferencedStudySequence);
        assertEquals("SQ", attr.vr().toString());

        attr = study.getAttribute(Tag.PatientName);
        assertEquals("MINT10^MINT10", attr.getValueAsString(null, 0));
        assertEquals("PN", attr.vr().toString());

        attr = study.getAttribute(Tag.PatientID);
        assertEquals("MINT10", attr.getValueAsString(null, 0));
        assertEquals("LO", attr.vr().toString());

        attr = study.getAttribute(Tag.PatientBirthDate);
        assertEquals("19910816", attr.getValueAsString(null, 0));
        assertEquals("DA", attr.vr().toString());

        attr = study.getAttribute(Tag.PatientBirthTime);
        assertEquals("TM", attr.vr().toString());

        attr = study.getAttribute(Tag.PatientSex);
        assertEquals("O", attr.getValueAsString(null, 0));
        assertEquals("CS", attr.vr().toString());

        attr = study.getAttribute(Tag.OtherPatientIDs);
        assertEquals("LO", attr.vr().toString());

        attr = study.getAttribute(Tag.OtherPatientNames);
        assertEquals("PN", attr.vr().toString());

        attr = study.getAttribute(Tag.PatientAge);
        assertEquals("AS", attr.vr().toString());

        attr = study.getAttribute(Tag.PatientSize);
        assertEquals("DS", attr.vr().toString());

        attr = study.getAttribute(Tag.PatientWeight);
        assertEquals("DS", attr.vr().toString());

        attr = study.getAttribute(Tag.EthnicGroup);
        assertEquals("SH", attr.vr().toString());

        attr = study.getAttribute(Tag.Occupation);
        assertEquals("SH", attr.vr().toString());

        attr = study.getAttribute(Tag.AdditionalPatientHistory);
        assertEquals("LT", attr.vr().toString());

        attr = study.getAttribute(Tag.PatientComments);
        assertEquals("LT", attr.vr().toString());

        attr = study.getAttribute(Tag.StudyInstanceUID);
        assertEquals("2.16.840.1.114255.393386351.1568457295.34445.4",
                attr.getValueAsString(null, 0));
        assertEquals("UI", attr.vr().toString());

        attr = study.getAttribute(Tag.StudyID);
        assertEquals("SH", attr.vr().toString());
    }
}
