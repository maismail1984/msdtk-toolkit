/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdtk.conversion;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.oht.miami.msdtk.conversion.dicom.Study2Dicom;
import org.oht.miami.msdtk.store.Store;
import org.oht.miami.msdtk.store.StoreFactory;
import org.oht.miami.msdtk.studymodel.Study;
import org.oht.miami.msdtk.util.DicomUID;

import org.dcm4che2.data.BasicDicomObject;
import org.dcm4che2.data.DicomElement;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.SimpleDicomElement;
import org.dcm4che2.data.Tag;
import org.dcm4che2.data.TransferSyntax;
import org.dcm4che2.data.VR;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

/**
 * Test if TransferSyntax is consistency when converting study or series into
 * dicom object
 * 
 * @author Jiefeng Zhai
 */
public class Study2DicomTest {

    private Study studyNormalized = null;

    private Store store = StoreFactory.createStore("testStoreFS.properties");

    private final String STUDY_UID_VALUE = "1.3.6.1.4.1.5962.1.3.10.2.1166562673.14401";

    private final String SERIES_UID_VALUE = "1.3.6.1.4.1.5962.1.3.10.2.1166562673.14401";

    private final String NEW_SERIES_UID_VALUE = "1.3.6.1.4.1.5962.1.3.10.2.1123562673.14401";

    private final String SOP_UID_VALUE = "1.3.6.1.4.1.5962.1.1.10.2.1.1166562673.14401";

    private final String NEW_SOP_UID_VALUE = "1.3.6.1.4.1.52222.1.1.10.2.1.1166562673.14401";

    @Before
    public void setUp() throws IOException {
        DicomElement studyUID = new SimpleDicomElement(Tag.StudyInstanceUID, VR.UI, true,
                STUDY_UID_VALUE.getBytes(), null);
        DicomElement seriesUID = new SimpleDicomElement(Tag.SeriesInstanceUID, VR.UI, true,
                SERIES_UID_VALUE.getBytes(), null);
        DicomElement sopUID = new SimpleDicomElement(Tag.SOPInstanceUID, VR.UI, true,
                SOP_UID_VALUE.getBytes(), null);
        DicomElement transferSyntax = new SimpleDicomElement(Tag.TransferSyntaxUID, VR.UI, true,
                TransferSyntax.ExplicitVRLittleEndian.uid().getBytes(), null);

        DicomObject obj = new BasicDicomObject();
        obj.add(sopUID);
        obj.add(studyUID);
        obj.add(seriesUID);
        obj.add(transferSyntax);

        studyNormalized = new Study(store, new DicomUID(STUDY_UID_VALUE), true, obj);
    }

    @After
    public void tearDown() {
        studyNormalized = null;
    }

    /**
     * Should throw exception if trying to convert study with series that have
     * different Tag.TransferSyntaxUID
     * 
     * @throws IOException
     */
    @Test
    public void testStudy2Dicom() throws IOException {
        DicomElement studyUID = new SimpleDicomElement(Tag.StudyInstanceUID, VR.UI, true,
                STUDY_UID_VALUE.getBytes(), null);
        DicomElement seriesUID = new SimpleDicomElement(Tag.SeriesInstanceUID, VR.UI, true,
                NEW_SERIES_UID_VALUE.getBytes(), null);
        DicomElement sopUID = new SimpleDicomElement(Tag.SOPInstanceUID, VR.UI, true,
                NEW_SOP_UID_VALUE.getBytes(), null);
        DicomElement transferSyntax = new SimpleDicomElement(Tag.TransferSyntaxUID, VR.UI, true,
                TransferSyntax.ExplicitVRBigEndian.uid().getBytes(), null);

        DicomObject obj = new BasicDicomObject();
        obj.add(sopUID);
        obj.add(studyUID);
        obj.add(seriesUID);
        obj.add(transferSyntax);

        assertEquals(studyNormalized.elementCount(), 4);
        assertEquals(studyNormalized.getSeries(SERIES_UID_VALUE).elementCount(), 2);
        assertEquals(studyNormalized.getInstance(SOP_UID_VALUE).elementCount(), 1);

        studyNormalized.addDicomObject(obj);

        assertEquals(studyNormalized.elementCount(), 7);
        assertEquals(studyNormalized.getSeries(SERIES_UID_VALUE).elementCount(), 3);
        assertEquals(studyNormalized.getSeries(NEW_SERIES_UID_VALUE).elementCount(), 3);
        assertEquals(studyNormalized.getInstance(SOP_UID_VALUE).elementCount(), 1);
        assertEquals(studyNormalized.getInstance(NEW_SOP_UID_VALUE).elementCount(), 1);

        try {
            Study2Dicom.study2UnifiedMultiSeriesDicom(studyNormalized);
            fail("Missing expected exception");
        } catch (IllegalArgumentException e) {
            assertEquals(e.getMessage(), "Could not convert Study with mixed transfer syntaxes");
        }

        DicomObject dicomObj = Study2Dicom.study2MultiSeriesDicom(studyNormalized);
        assertEquals(dicomObj.getString(Tag.TransferSyntaxUID),
                TransferSyntax.ExplicitVRLittleEndian.uid());
    }
}
