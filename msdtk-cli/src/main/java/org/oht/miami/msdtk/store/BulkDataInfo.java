/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdtk.store;

import org.oht.miami.msdtk.util.DicomUID;

import java.io.Serializable;
import java.util.UUID;

/**
 * The BulkDataInfo class implements a container for DICOM Data Element values
 * that are larger than DEValueThreshold for a single bulk data item. The values
 * are byte vectors contained in the contents field. Each value is separated by
 * the Multi-Part MIME separator. TODO : 1) Explain why the BulkDataSet is
 * separated from the StudyVersion. 2) Add a reference to the Multi-Part MIME
 * document. 2) Define a constant string that is the Multi-PartMIMESeparator.
 * 
 * @author James F Philbin (james.philbin@jhmi.edu)
 * @author Raphael Yu Ning
 */
public class BulkDataInfo implements Serializable {

    private static final long serialVersionUID = 7268177038410054345L;

    // TODO Decide if any of the following fields should be final
    // TODO document the following fields
    private final UUID uuid;

    private final DicomUID studyInstanceUID;

    //TODO: Add createdByVersionField so an info can be linked to its version
    public BulkDataInfo(DicomUID studyInstanceUID, UUID uuid) {
        this.uuid = uuid;
        this.studyInstanceUID = studyInstanceUID;
    }

    public UUID getUUID() {

        return uuid;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((studyInstanceUID == null) ? 0 : studyInstanceUID.hashCode());
        result = prime * result + ((uuid == null) ? 0 : uuid.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        BulkDataInfo other = (BulkDataInfo) obj;
        if (studyInstanceUID == null) {
            if (other.studyInstanceUID != null)
                return false;
        } else if (!studyInstanceUID.equals(other.studyInstanceUID))
            return false;
        if (uuid == null) {
            if (other.uuid != null)
                return false;
        } else if (!uuid.equals(other.uuid))
            return false;
        return true;
    }

}