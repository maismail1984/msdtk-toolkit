/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdtk.dicomio;

import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import org.oht.miami.msdtk.store.Store;

/**
 * Reads a DICOM study from files and builds a MS-DICOM study model.
 * 
 * @author Raphael Yu Ning
 */
public class FileBasedStudyReader extends StudyReader {

    private File inputFile = null;

    private FileFilter fileFilter = new DicomFileFilter();

    private File[] dicomFiles = null;

    private int currentDicomFileIndex = -1;

    private long studySize = 0;

    public FileBasedStudyReader(Store store) {

        super(store);
    }

    public FileBasedStudyReader(Store store, File inputFile) {

        super(store);
        setInputFile(inputFile);
    }

    public FileBasedStudyReader(Store store, String inputFilePath) {

        super(store);
        setInputFile(inputFilePath);
    }

    public File getInputFile() {

        return inputFile;
    }

    public void setInputFile(File inputFile) {

        this.inputFile = inputFile;
        dicomFiles = null;
        currentDicomFileIndex = -1;
    }

    public void setInputFile(String inputFilePath) {

        setInputFile(new File(inputFilePath));
    }

    public void setFileFilter(FileFilter fileFilter) {

        this.fileFilter = fileFilter;
    }

    public long getStudySize() {

        return studySize;
    }

    @Override
    protected boolean checkInput() {

        return inputFile != null;
    }

    @Override
    protected InputStream nextInputStream() throws IOException {

        if (currentDicomFileIndex < 0) {
            // try to treat inputFile as a directory first
            dicomFiles = inputFile.listFiles(fileFilter);

            if (dicomFiles == null) {
                if (inputFile.isFile()) {
                    // inputFile is a single file
                    dicomFiles = new File[] { inputFile };
                } else {
                    throw new FileNotFoundException("Invalid input file " + inputFile.toString());
                }
            }

            currentDicomFileIndex = 0;
        } else if (currentDicomFileIndex >= dicomFiles.length) {
            currentDicomFileIndex = 0;
            return null;
        }

        File dicomFile = dicomFiles[currentDicomFileIndex];

        if (currentDicomFileIndex == 0) {
            studySize = 0;
        }
        studySize += dicomFile.length();
        currentDicomFileIndex++;
        return new FileInputStream(dicomFile);
    }
}
