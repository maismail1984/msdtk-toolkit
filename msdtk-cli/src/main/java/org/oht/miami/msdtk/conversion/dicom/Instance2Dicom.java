/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdtk.conversion.dicom;

import org.oht.miami.msdtk.studymodel.BulkDataDicomElement;
import org.oht.miami.msdtk.studymodel.Instance;
import org.oht.miami.msdtk.studymodel.Study;

import org.dcm4che2.data.BasicDicomObject;
import org.dcm4che2.data.DicomElement;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.SimpleDicomElement;
import org.dcm4che2.data.Tag;
import org.dcm4che2.data.VR;
import org.dcm4che2.util.ByteUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

/**
 * Class to convert from study model instance object to dcm4che DICOM object.
 * 
 * @author Mahmoud Ismail(maismail@cs.jhu.edu)
 */
public class Instance2Dicom {

    /**
     * Converts a study model instance to a dcm4che DICOM object. (SFD) format.
     * 
     * @param instance
     *            the Instance to convert
     * @param frameSize
     * @param offset
     * @param pixelData
     * @return A list of converted DICOM objects.
     * @throws IOException
     */
    public static ArrayList<DicomObject> instance2SingleFrameDicomWithNoPixelData(
            Instance instance, int frameSize, int offset, byte[] pixelData) throws IOException {
        ArrayList<DicomObject> dcmObjs = new ArrayList<DicomObject>();
        // (TODO): instance.updateMediaStorage();
        DicomObject instanceDcmObj = new BasicDicomObject();
        Iterator<DicomElement> instanceAttrIter = instance.attributeIterator();
        /*
         * Adds all instance level attribute to the instance Dicom Object.
         */
        while (instanceAttrIter.hasNext()) {
            DicomElement currentEle = instanceAttrIter.next();
            if (currentEle.vr() == VR.BD) {
                BulkDataDicomElement bulkDataEle = (BulkDataDicomElement) currentEle;
                if (currentEle.tag() != Tag.PixelData) {
                    instanceDcmObj.putBytes(currentEle.tag(), bulkDataEle.getBulkDataVR(),
                            bulkDataEle.getBulkDataValueAsBytes(), bulkDataEle.bigEndian());
                } else {
                    System.arraycopy(bulkDataEle.getBulkDataValueAsBytes(), 0, pixelData, offset,
                            instance.getNumberOfFrames() * frameSize);
                }
            } else {
                instanceDcmObj.add(currentEle);
            }
        }

        if (instance.hasChildFrames()) {
            for (Iterator<Instance> instIter = instance.getChildrenFrames().iterator(); instIter
                    .hasNext();) {
                Instance childInstance = instIter.next();
                DicomObject childFrameDcmObj = new BasicDicomObject();
                Study2DicomUtil.copyDicomObject(instanceDcmObj, childFrameDcmObj);
                for (Iterator<DicomElement> childInstanceAttrIter = childInstance
                        .attributeIterator(); childInstanceAttrIter.hasNext();) {
                    DicomElement currentEle = childInstanceAttrIter.next();
                    // Instance frames should not have BDR DICOM elements
                    childFrameDcmObj.add(currentEle);
                }
                dcmObjs.add(childFrameDcmObj);
            }
        } else {
            dcmObjs.add(instanceDcmObj);
        }
        return dcmObjs;
    }

    /**
     * Converts a study model instance to a dcm4che DICOM object. (SFD) format.
     * 
     * @param seriesDcmObj
     *            The dcmObject that holds the series and study level attributes
     * @param instance
     *            The Instance to convert
     * @param withMultipleFrames
     *            Boolean flag, true implies creating a dicom object for each
     *            child frame in the input instance, false implies creating one
     *            output dicom object per input instance, child instance frames
     *            are added as child dicom object to the output dicom object.
     * @return A list of converted DICOM objects.
     * @throws IOException
     */
    public static ArrayList<DicomObject> instance2SingleFrameDicom(DicomObject seriesDcmObj,
            Instance instance, boolean withMultipleFrames) throws IOException {

        ArrayList<DicomObject> dcmObjs = new ArrayList<DicomObject>();
        DicomObject instanceDcmObj = new BasicDicomObject();
        Study2DicomUtil.copyDicomObject(seriesDcmObj, instanceDcmObj);
        Iterator<DicomElement> instanceAttrIter = instance.attributeIterator();
        /*
         * Adds all instance level attribute to the instance DICOM Object.
         */
        byte[] pixelData = null;
        VR pixelDataVr = null;
        while (instanceAttrIter.hasNext()) {
            DicomElement currentEle = instanceAttrIter.next();
            if (currentEle.vr() == VR.BD) {
                BulkDataDicomElement bulkDataEle = (BulkDataDicomElement) currentEle;
                if (!withMultipleFrames || bulkDataEle.tag() != Tag.PixelData
                        || !instance.hasChildFrames()) {
                    instanceDcmObj.putBytes(currentEle.tag(), bulkDataEle.getBulkDataVR(),
                            bulkDataEle.getBulkDataValueAsBytes(), bulkDataEle.bigEndian());
                } else {
                    // Reads the pixel data of all frames in a buffer.
                    pixelData = bulkDataEle.getBulkDataValueAsBytes();
                    pixelDataVr = bulkDataEle.getBulkDataVR();
                }
            } else {
                instanceDcmObj.add(currentEle);
            }
        }

        if (!withMultipleFrames) {
            // Adds child frames if any as a sequence of dicom objects.
            if (instance.hasChildFrames()) {
                DicomElement perFrameSequenceElement = instanceDcmObj
                        .get(Tag.PerFrameFunctionalGroupsSequence);
                if (perFrameSequenceElement == null) {
                    perFrameSequenceElement = instanceDcmObj
                            .putSequence(Tag.PerFrameFunctionalGroupsSequence);
                }
                for (Iterator<Instance> instIter = instance.getChildrenFrames().iterator(); instIter
                        .hasNext();) {
                    Instance childInstance = instIter.next();
                    DicomObject childFrame = new BasicDicomObject();

                    // Adds attributes within each frame.
                    for (Iterator<DicomElement> childInstanceAttrIter = childInstance
                            .attributeIterator(); childInstanceAttrIter.hasNext();) {
                        DicomElement dcmElement = childInstanceAttrIter.next();
                        // Frames should not have Dicom attributes of type BD
                        childFrame.add(dcmElement);
                    }
                    childFrame.setParent(instanceDcmObj);
                    perFrameSequenceElement.addDicomObject(childFrame);
                }
            }
            writeFileMetaInfo(instanceDcmObj);
            dcmObjs.add(instanceDcmObj);
        } else {

            // Makes sure the number of frames is removed when
            // writing a single DICOM file
            instanceDcmObj.remove(Tag.NumberOfFrames);
            // TODO: Make sure it is ok to remove this line.
            instanceDcmObj.remove(Tag.PerFrameFunctionalGroupsSequence);
            int frameIndex = 1;
            int frameSize = 1;
            if (instance.findAttribute(Tag.Rows) != null) {
                frameSize *= instance.findAttribute(Tag.Rows).getInt(false);
            }
            if (instance.findAttribute(Tag.Columns) != null) {
                frameSize *= instance.findAttribute(Tag.Columns).getInt(false);
            }
            if (instance.findAttribute(Tag.BitsAllocated) != null) {
                frameSize *= instance.findAttribute(Tag.BitsAllocated).getInt(false) / 8;
            }
            if (instance.findAttribute(Tag.SamplesPerPixel) != null) {
                frameSize *= instance.findAttribute(Tag.SamplesPerPixel).getInt(false);
            }
            if (instance.hasChildFrames()) {
                for (Instance frame : instance.getChildrenFrames()) {
                    DicomObject frameDcmObj = new BasicDicomObject();
                    Study2DicomUtil.copyDicomObject(instanceDcmObj, frameDcmObj);
                    Iterator<DicomElement> frameAttrIter = frame.attributeIterator();
                    while (frameAttrIter.hasNext()) {
                        DicomElement frameDcmElem = frameAttrIter.next();
                        // Instance frames should not have BDRs
                        frameDcmObj.add(frameDcmElem);
                    }

                    try {
                        byte[] framePixelData = Arrays.copyOfRange(pixelData, (frameIndex - 1)
                                * frameSize, frameIndex * frameSize);
                        frameDcmObj.putBytes(Tag.PixelData, pixelDataVr, framePixelData);
                    } catch (Exception ex) {
                        System.out.println("Error: data out of range./"
                                + " Unable to save pixel data for the frame");
                    }

                    String instanceSOPInstanceUID = instance.getSOPInstanceUID();
                    DicomElement sopInstanceUidDicomElement = instance
                            .getAttribute(Tag.SOPInstanceUID);
                    if (sopInstanceUidDicomElement != null) {
                        if (frameDcmObj.get(Tag.SOPInstanceUID).getValueAsString(null, 0)
                                .compareTo(instanceSOPInstanceUID) == 0) {
                            frameDcmObj.remove(Tag.SOPInstanceUID);
                            frameDcmObj.add(new SimpleDicomElement(Tag.SOPInstanceUID,
                                    sopInstanceUidDicomElement.vr(), sopInstanceUidDicomElement
                                            .bigEndian(),
                                    (instanceSOPInstanceUID + "." + frameIndex).getBytes(), false));
                        }
                    }
                    writeFileMetaInfo(frameDcmObj);

                    dcmObjs.add(frameDcmObj);
                    frameIndex++;
                }
            } else {
                // Adds instanceDcmObj to the output list if it has no child
                // frames.
                writeFileMetaInfo(instanceDcmObj);
                dcmObjs.add(instanceDcmObj);
            }
        }
        return dcmObjs;
    }

    /**
     * Write study file meta information to dicom object
     * 
     * @param instanceDcmObj
     *            dicom object to add FMI to
     */
    private static void writeFileMetaInfo(DicomObject instanceDcmObj) {
        // Set File Meta Information Version to 1 per the FMI documentation
        instanceDcmObj.putBytes(Tag.FileMetaInformationVersion, VR.OB,
                ByteUtils.ushort2bytesBE(1, new byte[2], 0));
        // Set Implementation Class UID to a special UID for MSD-Toolkit
        instanceDcmObj.putString(Tag.ImplementationClassUID, VR.UI, Study.IMPLEMENTATION_CLASS_UID);
        // Set Implementation Version Name to the version number of MSD-Toolkit
        instanceDcmObj.putString(Tag.ImplementationVersionName, VR.SH,
                Study.IMPLEMENTATION_VERSION_NAME);
        // Set Media Storage SOP Instance UID to Study Instance UID
        instanceDcmObj.putString(Tag.MediaStorageSOPInstanceUID, VR.UI,
                instanceDcmObj.getString(Tag.SOPInstanceUID));
        // set MediaStorageSOPClassUID to the value of the SOPClassUID
        String sopClassUID = instanceDcmObj.getString(Tag.SOPClassUID);
        instanceDcmObj.putString(Tag.MediaStorageSOPClassUID, VR.UI, sopClassUID);
        // set the source application entity title to the MSD toolkit
        instanceDcmObj.putString(Tag.SourceApplicationEntityTitle, VR.AE, Study.SOURCE_AET);
    }

}