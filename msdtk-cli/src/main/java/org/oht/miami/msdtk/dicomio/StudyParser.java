package org.oht.miami.msdtk.dicomio;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.UUID;

import org.dcm4che2.data.DicomElement;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.SequenceDicomElement;
import org.dcm4che2.data.Tag;
import org.dcm4che2.data.TransferSyntax;
import org.dcm4che2.data.UID;
import org.dcm4che2.data.VR;
import org.dcm4che2.io.DicomCodingException;
import org.dcm4che2.io.DicomInputHandler;
import org.dcm4che2.io.DicomInputStream;
import org.oht.miami.msdtk.store.BulkDataInfo;
import org.oht.miami.msdtk.store.Store;
import org.oht.miami.msdtk.store.VersionType;
import org.oht.miami.msdtk.studymodel.BulkDataDicomElement;
import org.oht.miami.msdtk.studymodel.BulkDataReference;
import org.oht.miami.msdtk.studymodel.Study;
import org.oht.miami.msdtk.util.DicomUID;

/**
 * Parses DICOM streams into studies.
 * 
 * @author Raphael Yu Ning
 */
public class StudyParser {

    protected final ActiveStudyTable activeStudyTable;

    protected final Store store;

    private int bulkDataThreshold = 256;

    /**
     * Constructs a {@code StudyParser} that uses the given active study table
     * and the MSD-Store provided by the active study table.
     * 
     * @param activeStudyTable
     *            The active study table to be used by this parser. Must not be
     *            {@code null}.
     */
    public StudyParser(ActiveStudyTable activeStudyTable) {

        if (activeStudyTable == null) {
            throw new IllegalArgumentException("ActiveStudyTable must not be null");
        }
        this.activeStudyTable = activeStudyTable;
        this.store = activeStudyTable.getStore();
    }

    /**
     * Constructs a {@code StudyParser} that uses the given MSD-Store and no
     * active study table, hence is only suitable for parsing a single study.
     * 
     * @param store
     *            The MSD-Store to be used by this parser.
     */
    public StudyParser(Store store) {

        if (store == null) {
            throw new IllegalArgumentException("Store must not be null");
        }
        activeStudyTable = null;
        this.store = store;
    }

    /**
     * Get bulk data threshold
     * 
     * @return bulk data threshold
     */
    public int getBulkDataThreshold() {

        return bulkDataThreshold;
    }

    /**
     * Set bulk data threshold
     * 
     * @param bulkDataThreshold
     *            bulk data threshold
     */
    public void setBulkDataThreshold(int bulkDataThreshold) {

        if (bulkDataThreshold < 0) {
            throw new IllegalArgumentException("Bulk Data threshold must be non-negative");
        }
        this.bulkDataThreshold = bulkDataThreshold;
    }

    /**
     * Parses the given {@code DicomInputStream} and adds the parsed DICOM
     * attributes to a {@code Study} object returned by
     * {@link Store#findOrCreateStudy(DicomUID)}. This is equivalent to
     * {@code parse(dicomIn, attributeSet, null)}.
     * 
     * @param dicomIn
     *            The input DICOM stream.
     * @param attributeSet
     *            A set of attributes which complements {@code dicomIn} (e.g.
     *            Media Storage SOP Class UID, Media Storage SOP Instance UID
     *            and Transfer Syntax UID could be sent in a separate message).
     *            If {@code null}, no extra attributes are added to
     *            {@code studyVersion}.
     * @return The {@code Study} object that contains the parsed attributes.
     * @throws DicomCodingException
     * @throws IOException
     * @see #parse(DicomInputStream, DicomObject, Study)
     */
    public Study parse(DicomInputStream dicomIn, DicomObject attributeSet)
            throws DicomCodingException, IOException {

        return parse(dicomIn, attributeSet, null);
    }

    /**
     * Parses the given {@code DicomInputStream} and adds the parsed DICOM
     * attributes to the given {@code Study} object. If the {@code Study} object
     * is {@code null}, this method will use the {@code Study} object returned
     * by {@link Store#findOrCreateStudy(DicomUID)}.
     * 
     * @param dicomIn
     *            The input DICOM stream.
     * @param attributeSet
     *            A set of attributes which complements {@code dicomIn} (e.g.
     *            Media Storage SOP Class UID, Media Storage SOP Instance UID
     *            and Transfer Syntax UID could be sent in a separate message).
     *            If {@code null}, no extra attributes are added to
     *            {@code studyVersion}.
     * @param studyVersion
     *            An existing {@code Study} object to hold the parsed
     *            attributes, or {@code null}.
     * @return The {@code Study} object that contains the parsed attributes.
     * @throws DicomCodingException
     * @throws IOException
     */
    public Study parse(DicomInputStream dicomIn, DicomObject attributeSet, Study studyVersion)
            throws DicomCodingException, IOException {

        StoppableDicomInputHandler smallAttributeHandler = new StoppableDicomInputHandler();
        dicomIn.setHandler(smallAttributeHandler);
        smallAttributeHandler.setStopTag(Tag.MediaStorageSOPClassUID);
        if (attributeSet == null) {
            attributeSet = dicomIn.readFileMetaInformation();
        } else {
            dicomIn.readFileMetaInformation(attributeSet);
        }
        String mediaStorageSOPClassUID = attributeSet.getString(Tag.MediaStorageSOPClassUID);
        if (UID.MultiSeriesStudyStorage.equals(mediaStorageSOPClassUID)) {
            studyVersion = parseMultiSeries(dicomIn, attributeSet, studyVersion);
        } else {
            studyVersion = parseNonMultiSeries(dicomIn, attributeSet, studyVersion);
        }
        studyVersion.addDicomObject(attributeSet);
        return studyVersion;
    }

    /**
     * Parses the given {@code DicomInputStream} and adds the parsed DICOM
     * attributes to the given {@code Study} object. This is equivalent to
     * {@code parse(dicomIn, null, studyVersion)}.
     * 
     * @param dicomIn
     *            The input DICOM stream.
     * @param studyVersion
     *            An existing {@code Study} object to hold the parsed
     *            attributes, or {@code null}.
     * @return The {@code Study} object that contains the parsed attributes.
     * @throws DicomCodingException
     * @throws IOException
     * @see #parse(DicomInputStream, DicomObject, Study)
     */
    public Study parse(DicomInputStream dicomIn, Study studyVersion) throws DicomCodingException,
            IOException {

        return parse(dicomIn, null, studyVersion);
    }

    /**
     * Parses the given {@code DicomInputStream} and adds the parsed DICOM
     * attributes to a {@code Study} object returned by
     * {@link Store#findOrCreateStudy(DicomUID)}. This is equivalent to
     * {@code parse(dicomIn, null, null)}.
     * 
     * @param dicomIn
     *            The input DICOM stream.
     * @return The {@code Study} object that contains the parsed attributes.
     * @throws DicomCodingException
     * @throws IOException
     * @see #parse(DicomInputStream, DicomObject, Study)
     */
    public Study parse(DicomInputStream dicomIn) throws DicomCodingException, IOException {

        return parse(dicomIn, null, null);
    }

    // The following methods are protected to allow subclass extensibility

    /**
     * Parses the given {@code DicomInputStream}, which is assumed to be in the
     * standard DICOM format, and adds the parsed DICOM attributes to the given
     * {@code Study} object.
     * 
     * @param dicomIn
     *            The input DICOM stream.
     * @param attributeSet
     *            A set of attributes (e.g. File Meta Elements) which
     *            complements {@code dicomIn}.
     * @param studyVersion
     *            An existing {@code Study} object to hold the parsed
     *            attributes, or {@code null}.
     * @return The {@code Study} object that contains the parsed attributes.
     * @throws DicomCodingException
     * @throws IOException
     */
    protected Study parseNonMultiSeries(DicomInputStream dicomIn, DicomObject attributeSet,
            Study studyVersion) throws DicomCodingException, IOException {

        StoppableDicomInputHandler smallAttributeHandler = new StoppableDicomInputHandler();
        dicomIn.setHandler(smallAttributeHandler);
        smallAttributeHandler.setStopTag(Tag.StudyInstanceUID);
        dicomIn.readDicomObject(attributeSet, -1);

        String studyInstanceUID = attributeSet.getString(Tag.StudyInstanceUID);
        if (studyInstanceUID == null) {
            throw new DicomCodingException("Invalid DICOM object: "
                    + "Missing (0020,000D) Study Instance UID");
        }
        if (studyVersion == null) {
            DicomUID validatedStudyInstanceUID = null;
            try {
                validatedStudyInstanceUID = new DicomUID(studyInstanceUID);
            } catch (Exception e) {
                throw new DicomCodingException("Invalid DICOM object: "
                        + "Invalid (0020,000D) Study Instance UID");
            }
            if (activeStudyTable != null) {
                studyVersion = activeStudyTable.findOrCreateStudy(validatedStudyInstanceUID);
            } else {
                studyVersion = store.findOrCreateStudy(validatedStudyInstanceUID);
            }
        }

        BulkDataAttributeHandler bulkDataAttributeHandler = new BulkDataAttributeHandler();
        dicomIn.setHandler(bulkDataAttributeHandler);
        bulkDataAttributeHandler.setStudyVersion(studyVersion);
        dicomIn.readDicomObject(attributeSet, -1);
        return studyVersion;
    }

    /**
     * Parses the given {@code DicomInputStream}, which is assumed to be in the
     * MS-DICOM format, and adds the parsed DICOM attributes to the given
     * {@code Study} object.
     * 
     * @param dicomIn
     *            The input DICOM stream.
     * @param attributeSet
     *            A set of attributes (e.g. File Meta Elements) which
     *            complements {@code dicomIn}.
     * @param studyVersion
     *            An existing {@code Study} object to hold the parsed
     *            attributes, or {@code null}.
     * @return The {@code Study} object that contains the parsed attributes.
     * @throws DicomCodingException
     * @throws IOException
     */
    protected Study parseMultiSeries(DicomInputStream dicomIn, DicomObject attributeSet,
            Study studyVersion) throws DicomCodingException, IOException {

        StoppableDicomInputHandler smallAttributeHandler = new StoppableDicomInputHandler();
        dicomIn.setHandler(smallAttributeHandler);
        smallAttributeHandler.setStopTag(Tag.PrivateInformation);
        dicomIn.readFileMetaInformation(attributeSet);

        String mediaStorageSOPInstanceUID = attributeSet.getString(Tag.MediaStorageSOPInstanceUID);
        if (mediaStorageSOPInstanceUID == null) {
            throw new DicomCodingException("Invalid MS-DICOM object: "
                    + "Missing (0002,0003) Media Storage SOP Instance UID");
        }
        if (studyVersion == null) {
            DicomUID validatedStudyInstanceUID = null;
            try {
                validatedStudyInstanceUID = new DicomUID(mediaStorageSOPInstanceUID);
            } catch (Exception e) {
                throw new DicomCodingException("Invalid MS-DICOM object: "
                        + "Invalid (0002,0003) Media Storage SOP Instance UID");
            }
            // TODO Should call Store.findOrCreateStudy() when parsing an
            // untrusted MSD study
            studyVersion = new Study(store, validatedStudyInstanceUID);
            studyVersion.setVersionType(VersionType.Unchanged);
        }
        parsePrivateInformation(attributeSet, studyVersion);

        MultiSeriesDicomInputHandler multiSeriesDicomHandler = new MultiSeriesDicomInputHandler();
        dicomIn.setHandler(multiSeriesDicomHandler);
        multiSeriesDicomHandler.setStudyVersion(studyVersion);
        dicomIn.readDicomObject(attributeSet, -1);
        return studyVersion;
    }

    /**
     * Parse private information
     * 
     * @param fmiAttributes
     *            dicom object to get attributes from
     * @param studyVersion
     *            to add attributes to
     * @throws IOException
     */
    protected void parsePrivateInformation(DicomObject fmiAttributes, Study studyVersion)
            throws IOException {

        String privateInfoCreatorUID = fmiAttributes.getString(Tag.PrivateInformationCreatorUID);
        if (!privateInfoCreatorUID.equals(Study.IMPLEMENTATION_CLASS_UID)) {
            throw new DicomCodingException("Invalid MS-DICOM object: "
                    + "Unexpected (0002,0100) Private Information Creator UID \""
                    + privateInfoCreatorUID + "\". Expected \"" + Study.IMPLEMENTATION_CLASS_UID
                    + "\"");
        }

        DicomElement privateInfoSq = convertFileMetaPrivateToSequence(fmiAttributes);
        if (privateInfoSq == null)
            throw new DicomCodingException(
                    "Invalid MS-DICOM object: Tag (0002,0102) Private Information not found");
        DicomObject privateInfoItem = privateInfoSq.getDicomObject();
        if (privateInfoItem == null) {
            throw new DicomCodingException(
                    "Invalid MS-DICOM object: No item found in (0002,0102) Private Information ");
        }

        // Bulk Data Sequence
        // TODO: Handle local MSD differently by reconstructing the bulkdataset
        // without parsing
        parseBulkDataSequence(studyVersion, privateInfoItem);
    }

    /**
     * Parse bulk data sequence
     * 
     * @param studyVersion
     *            to parse to
     * @param privateInfoItem
     *            object to pull attributes from
     * @throws DicomCodingException
     */
    protected void parseBulkDataSequence(Study studyVersion, DicomObject privateInfoItem)
            throws DicomCodingException {

        DicomElement bulkDataSequence = privateInfoItem.get(Tag.BulkDataSequence);
        if (bulkDataSequence != null) {
            int numberOfBulkDataObjects = bulkDataSequence.countItems();
            for (int i = 0; i < numberOfBulkDataObjects; i++) {
                DicomObject bulkDataItem = bulkDataSequence.getDicomObject(i);
                String bulkDataUUIDValue = bulkDataItem.getString(Tag.BulkDataUUID);
                if (bulkDataUUIDValue == null) {
                    throw new DicomCodingException("Invalid MS-DICOM object: "
                            + "Missing UUID for Bulk Data #" + i);
                }
                UUID bulkDataUUID = null;
                try {
                    bulkDataUUID = UUID.fromString(bulkDataUUIDValue);
                } catch (IllegalArgumentException e) {
                    throw new DicomCodingException("Invalid MS-DICOM object: " + "Invalid UUID \""
                            + bulkDataUUIDValue + "\" for Bulk Data #" + i);
                }
                BulkDataInfo bulkDataInfo = new BulkDataInfo(studyVersion.getStudyInstanceUID(),
                        bulkDataUUID);
                studyVersion.getBulkDataSet().addInfo(bulkDataInfo);
            }
            privateInfoItem.remove(Tag.BulkDataSequence);
        }
    }

    /**
     * Parse out the private information bytes, convert to a sequence and add to
     * dicom object
     * 
     * @param fmiAttributes
     *            dicom object containing byte array
     * @return dicom sequence parsed from dicom object
     * @throws IOException
     */
    protected DicomElement convertFileMetaPrivateToSequence(DicomObject fmiAttributes)
            throws IOException {

        DicomElement privateInfoAttribute = fmiAttributes.get(Tag.PrivateInformation);
        if (privateInfoAttribute == null) {
            return null;
        }

        // parse out private information bytes and convert to sequence
        byte[] privateInfoBytes = privateInfoAttribute.getBytes();
        ByteArrayInputStream bytesIn = new ByteArrayInputStream(privateInfoBytes);
        DicomInputStream dicomIn = new DicomInputStream(bytesIn,
                TransferSyntax.ExplicitVRLittleEndian);
        DicomElement privateInfoSq = new SequenceDicomElement(Tag.PrivateInformation, VR.SQ, false,
                new ArrayList<Object>(), null);
        try {
            dicomIn.readItems(privateInfoSq, privateInfoBytes.length);
        } finally {
            dicomIn.close();
        }

        fmiAttributes.remove(Tag.PrivateInformation);

        // add private information sequence to DICOM object
        fmiAttributes.add(privateInfoSq);
        return privateInfoSq;
    }

    protected class StoppableDicomInputHandler implements DicomInputHandler {

        protected Integer stopTag = null;

        public void setStopTag(Integer stopTag) {

            this.stopTag = stopTag;
        }

        @Override
        public boolean readValue(DicomInputStream dicomIn) throws IOException {

            dicomIn.readValue(dicomIn);
            return !shouldStop(dicomIn.level(), dicomIn.tag());
        }

        protected boolean shouldStop(int currentLevel, int currentTag) {

            return currentLevel == 0 && stopTag != null && currentTag >= stopTag;
        }
    }

    protected class MultiSeriesDicomInputHandler extends StoppableDicomInputHandler {

        protected Study studyVersion = null;

        public void setStudyVersion(Study studyVersion) {

            this.studyVersion = studyVersion;
        }

        @Override
        public boolean readValue(DicomInputStream dicomIn) throws IOException {

            int tag = dicomIn.tag();
            VR vr = dicomIn.vr();
            int valueLength = dicomIn.valueLength();
            boolean bigEndian = dicomIn.getTransferSyntax().bigEndian();
            DicomObject attributeSet = dicomIn.getDicomObject();

            switch (tag) {
            case Tag.Item:
            case Tag.ItemDelimitationItem:
            case Tag.SequenceDelimitationItem:
                dicomIn.readValue(dicomIn);
                break;

            default:
                if (vr == VR.SQ) {
                    DicomElement datasetSequence = attributeSet.putSequence(tag);
                    dicomIn.readItems(datasetSequence, valueLength);
                } else if (valueLength == -1) {
                    DicomElement fragmentSequence = attributeSet.putFragments(tag, vr, bigEndian);
                    dicomIn.readItems(fragmentSequence, valueLength);
                } else if (vr == VR.BD) {
                    BulkDataReference bdr = BulkDataReference.fromBytes(
                            dicomIn.readBytes(valueLength), bigEndian,
                            studyVersion.getBulkDataSet());
                    attributeSet.add(new BulkDataDicomElement(tag, bigEndian, bdr));
                } else {
                    DicomElement attribute = attributeSet.putBytes(tag, vr,
                            dicomIn.readBytes(valueLength), bigEndian);
                    if (tag == Tag.FileMetaInformationGroupLength) {
                        dicomIn.setEndOfFileMetaInfoPosition(dicomIn.getStreamPosition()
                                + attribute.getInt(false));
                    }
                }
            }
            return !this.shouldStop(dicomIn.level(), tag);
        }
    }

    protected class BulkDataAttributeHandler extends StoppableDicomInputHandler {

        protected Study studyVersion = null;

        public void setStudyVersion(Study studyVersion) {

            this.studyVersion = studyVersion;
        }

        @Override
        public boolean readValue(DicomInputStream dicomIn) throws IOException {

            int tag = dicomIn.tag();
            VR vr = dicomIn.vr();
            int valueLength = dicomIn.valueLength();
            boolean bigEndian = dicomIn.getTransferSyntax().bigEndian();
            DicomObject attributeSet = dicomIn.getDicomObject();

            switch (tag) {
            case Tag.Item:
            case Tag.ItemDelimitationItem:
            case Tag.SequenceDelimitationItem:
                dicomIn.readValue(dicomIn);
                break;

            default:
                if (vr == VR.SQ) {
                    DicomElement datasetSequence = attributeSet.putSequence(tag);
                    dicomIn.readItems(datasetSequence, valueLength);
                } else if (valueLength == -1) {
                    DicomElement fragmentSequence = attributeSet.putFragments(tag, vr, bigEndian);
                    dicomIn.readItems(fragmentSequence, valueLength);
                } else if (valueLength > bulkDataThreshold) {
                    BulkDataReference bdr = studyVersion.appendBulkDataValue(vr,
                            dicomIn.readBytes(valueLength));
                    attributeSet.add(new BulkDataDicomElement(tag, bigEndian, bdr));
                } else {
                    DicomElement attribute = attributeSet.putBytes(tag, vr,
                            dicomIn.readBytes(valueLength), bigEndian);
                    if (tag == Tag.FileMetaInformationGroupLength) {
                        dicomIn.setEndOfFileMetaInfoPosition(dicomIn.getStreamPosition()
                                + attribute.getInt(false));
                    }
                }
            }
            return !this.shouldStop(dicomIn.level(), tag);
        }
    }
}
