Toolkit to read DICOM studies. It has de-identification framework that conforms with the DICOM standard. The toolkit removes repeated DICOM attributes in the input studies. It supports the conversion between  single-frame-dicom, combined study object and multi-series format.

The binaries are available at /bin/
The source code is available and can be built using the steps below.

Build Steps using linux/mac machine
**********************
**********************
1. create a local directory
>   mkdir mydir
2. Initialize the git directory
>  git init
3. Point your local git workspace to the project git repository
>  git remote add origin https://maismail1984@bitbucket.org/maismail1984/msdtk-toolkit.git
4. pull the source code
>  git pull
5. Ensure JAVA_HOME is referring to your java home directory
>  Example: echo $JAVA_HOME
>  export JAVA_HOME=/Library/Java/JavaVirtualMachines/1.7.0.jdk/Contents/Home
6. Build the following project in the following sequence
        6.1 msdtk-test-data
        >  mvn install
        6.2 msdtk-core
        > mvn install -DskipTests=true  // Skips running the unit tests. The unit tests run through eclipse but fail from the command line
        6.3 msdtk-cli
        > mvn package