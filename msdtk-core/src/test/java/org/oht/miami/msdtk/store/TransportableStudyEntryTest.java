/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdtk.store;

import static org.junit.Assert.*;

import org.oht.miami.msdtk.studymodel.Study;
import org.oht.miami.msdtk.util.TestUtils;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.UUID;

/**
 * Class for testing transportable study entry
 * 
 * @author Ryan Stonebraker(rstonebr@harris.com)
 */
public class TransportableStudyEntryTest {

    private StoreFS store;

    private Study studyMINT10;

    private static final String ROOT_DIRECTORY = "target" + File.separator + "test-out"
            + File.separator + "TransportableStudyEntryTest";
    private static final String OUTPUT_FILE_NAME = ROOT_DIRECTORY + File.separator + "output.SEM";

    private static final UUID uuid = UUID.randomUUID();

    @Before
    public void setUp() throws IOException {
        store = new StoreFS(ROOT_DIRECTORY);
        studyMINT10 = TestUtils.readStudyFromResource(store, "MINT10");
    }

    @After
    public void tearDown() throws IOException {
        TestUtils.cleanStore(store);
    }

    @Test
    public void testSerialization() throws IOException, ClassNotFoundException {
        store.writeVersion(studyMINT10);
        TransportableStudyEntry transportableStudyEntry = store.getTransportableStudyEntry(studyMINT10.getStudyInstanceUID());
        FileOutputStream fos = new FileOutputStream(OUTPUT_FILE_NAME);
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        try {
            oos.writeObject(transportableStudyEntry);
            oos.flush();
        } finally {
            oos.close();
        }
        FileInputStream fis = new FileInputStream(OUTPUT_FILE_NAME);
        ObjectInputStream ois = new ObjectInputStream(fis);
        TransportableStudyEntry transportableStudyEntryDeserialized;
        try {
            transportableStudyEntryDeserialized = (TransportableStudyEntry) ois.readObject();
        } finally {
            ois.close();
        }
        assertEquals(transportableStudyEntry, transportableStudyEntryDeserialized);
    }
}
