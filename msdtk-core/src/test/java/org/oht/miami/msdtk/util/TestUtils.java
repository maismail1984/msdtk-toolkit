/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdtk.util;

import org.oht.miami.msdtk.dicomio.FileBasedStudyReader;
import org.oht.miami.msdtk.dicomio.ResourceBasedStudyReader;
import org.oht.miami.msdtk.dicomio.StudyReader;
import org.oht.miami.msdtk.store.Store;
import org.oht.miami.msdtk.store.StoreFS;
import org.oht.miami.msdtk.studymodel.Study;
import org.oht.miami.msdtk.testing.TestDataLocator;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class TestUtils {

    private TestUtils() {
    }

    public static Study readStudyFromResource(Store store, String studyName) throws IOException {
        TestDataLocator testDataLocator = new TestDataLocator();
        List<String> testStudyResources = testDataLocator.locateStudy(studyName);
        StudyReader studyReader = new ResourceBasedStudyReader(store, testStudyResources);
        return studyReader.readStudy();
    }

    public static List<Study> readStudiesFromResource(Store store, String studyName)
            throws IOException {
        TestDataLocator testDataLocator = new TestDataLocator();
        List<String> testStudyResources = testDataLocator.locateStudy(studyName);
        StudyReader studyReader = new ResourceBasedStudyReader(store, testStudyResources);
        return studyReader.readStudies();
    }

    public static Study readStudyFromFile(Store store, File inputFile) throws IOException {
        StudyReader studyReader = new FileBasedStudyReader(store, inputFile);
        return studyReader.readStudy();
    }

    public static File writeMultiSeries(Study study, File outputDir, String studyName)
            throws IOException {
        File multiSeriesOutputDir = new File(outputDir, studyName + "_MSD");
        multiSeriesOutputDir.mkdir();
        Study2Dicom.study2MultiSeriesDicom(study, multiSeriesOutputDir);
        return multiSeriesOutputDir;
    }

    public static File writeSingleFrame(Study study, File outputDir, String studyName,
            boolean withMultipleFrame) throws IOException {
        File singleFrameOutputDir = new File(outputDir, studyName + "_SFD");
        singleFrameOutputDir.mkdir();
        Study2Dicom.study2SingleFrameDicom(study, singleFrameOutputDir, withMultipleFrame);
        return singleFrameOutputDir;
    }

    public static File writeMultiFrame(Study study, File outputDir, String studyName)
            throws IOException {
        File multiFrameOutputDir = new File(outputDir, studyName + "_MFD");
        multiFrameOutputDir.mkdir();
        Study2Dicom.study2MultiFrameDicom(study, multiFrameOutputDir);
        return multiFrameOutputDir;
    }

    public static void cleanStore(Store store) throws IOException {
        if (store instanceof StoreFS) {
            StoreFS storefs = (StoreFS) store;
            if (storefs.getRootDirectory().exists()) {
                FileUtils.deleteFile(storefs.getRootDirectory());
            }
        }
    }

}
