/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdtk.dicomio;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.ListIterator;

import org.oht.miami.msdtk.store.Store;

/**
 * Reads a DICOM study from classpath resources and builds a MS-DICOM study
 * model.
 * 
 * @author Raphael Yu Ning
 */
public class ResourceBasedStudyReader extends StudyReader {

    private List<String> inputResources = new ArrayList<String>();

    private ListIterator<String> inputIterator = null;

    public ResourceBasedStudyReader(Store store) {

        super(store);
    }

    public ResourceBasedStudyReader(Store store, String inputResource) {

        super(store);
        addInputResource(inputResource);
    }

    public ResourceBasedStudyReader(Store store, Collection<String> inputResources) {

        super(store);
        addInputResources(inputResources);
    }

    public List<String> getInputResources() {

        return inputResources;
    }

    public void addInputResource(String inputResource) {

        inputResources.add(inputResource);
    }

    public void addInputResources(Collection<String> inputResources) {

        this.inputResources.addAll(inputResources);
    }

    public void resetInputResources() {

        inputResources.clear();
    }

    @Override
    protected boolean checkInput() {

        return !inputResources.isEmpty();
    }

    @Override
    protected InputStream nextInputStream() throws IOException {

        if (inputIterator == null) {
            inputIterator = inputResources.listIterator();
        } else if (!inputIterator.hasNext()) {
            inputIterator = null;
            return null;
        }

        String resourceName = inputIterator.next();
        if (resourceName.startsWith("classpath:")) {
            // Removes "classpath:" from resource name
            resourceName = resourceName.substring("classpath:".length());
        }

        return Thread.currentThread().getContextClassLoader().getResourceAsStream(resourceName);
    }
}
