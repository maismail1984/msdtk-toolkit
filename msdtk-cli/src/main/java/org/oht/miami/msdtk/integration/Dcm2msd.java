/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdtk.integration;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;

import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.dcm4che2.io.DicomInputStream;
import org.dcm4che2.io.DicomOutputStream;
import org.oht.miami.msdtk.deidentification.StudyProfileOptions;
import org.oht.miami.msdtk.dicomio.FileBasedStudyReader;
import org.oht.miami.msdtk.dicomio.StudyReader;
import org.oht.miami.msdtk.store.Store;
import org.oht.miami.msdtk.store.StoreFactory;
import org.oht.miami.msdtk.studymodel.Instance;
import org.oht.miami.msdtk.studymodel.Series;
import org.oht.miami.msdtk.studymodel.Study;
import org.oht.miami.msdtk.util.Study2Dicom;

public class Dcm2msd {

    private static final Store store = StoreFactory.createStore();

    /*
     * args[0]: input directory args[1]: output directory args[2]: method
     * args[3]: normalize
     */
    public static void main(String[] args) throws IOException, InterruptedException {

    	checkInput(args);
        Study study = null;
        final String inputDir = args[0];
        final String outputDir = args[1];
        final String method = args[2];
        final String normalize = args[3];
        if (!normalize.equalsIgnoreCase("normalize") && !normalize.equalsIgnoreCase("no-normalize")) {
            System.err.println("Invalid normalize argument: " + normalize);
            printUsage();
            return;
        }
        final File in = new File(inputDir);
        final File out = new File(outputDir + "/" + in.getName());

        final long startTime;
        final long entTime;
        cleanOutFolder(out);

        // Reads dicom input and build its study model
        if (method.equalsIgnoreCase("read_dicom")) {
            startTime = System.currentTimeMillis();
            study = readStudy(in);
            entTime = System.currentTimeMillis();
            
        // Read dicom input into dcm4che dicom object
        } else if (method.equalsIgnoreCase("read_dcmObj")) {
            startTime = System.currentTimeMillis();
            createDicomObjects(in);
            entTime = System.currentTimeMillis();
            
        } else if (method.equalsIgnoreCase("write_study_multiframe")) {
            study = readFromDicom(in, out, normalize.equalsIgnoreCase("normalize"));
            Thread.sleep(1000);
            startTime = System.currentTimeMillis();
            Study2Dicom.study2MultiSeriesDicom(study, out);
            entTime = System.currentTimeMillis();
        } else if (method.equalsIgnoreCase("write_series_multiframe")) {
            study = readFromDicom(in, out, normalize.equalsIgnoreCase("normalize"));
            Thread.sleep(1000);
            startTime = System.currentTimeMillis();
            Study2Dicom.study2MultiFrameDicom(study, out);
            entTime = System.currentTimeMillis();
        } else if (method.equalsIgnoreCase("write_composite")) {
            study = readFromDicom(in, out, normalize.equalsIgnoreCase("normalize"));
            Thread.sleep(1000);
            startTime = System.currentTimeMillis();
            Study2Dicom.study2SingleFrameDicom(study, out, true);
            entTime = System.currentTimeMillis();
        } else if (method.equalsIgnoreCase("write_msd")) {
            study = readStudy(in);
            Thread.sleep(1000);
            startTime = System.currentTimeMillis();
            writeStudy(study, out);
            entTime = System.currentTimeMillis();
        } else if (method.equalsIgnoreCase("write_sfd")) {
            startTime = System.currentTimeMillis();
            study = readStudy(in);
            Study2Dicom.study2SingleFrameDicom(study, out, false);
            entTime = System.currentTimeMillis();
        } else if (method.equalsIgnoreCase("study_report")) {
            startTime = System.currentTimeMillis();
            createStudyReport(in, out);
            entTime = System.currentTimeMillis();
        } else if (method.equalsIgnoreCase("clean")) {
            startTime = System.currentTimeMillis();
            StudyReader reader = new FileBasedStudyReader(store, inputDir);
            Study inputStudy = reader.readStudy();
            StudyProfileOptions pOptions = new StudyProfileOptions();
            inputStudy.deIdentifyStudy(pOptions);
            System.out.println("Deideintification option = " + pOptions.isBasicProfile());
            Study2Dicom.study2SingleFrameDicom(inputStudy, out, true);
            entTime = System.currentTimeMillis();
        } else {
            System.err.println("Unrecognized method " + method);
            printUsage();
            return;
        }

        /* ignore initial run if using cached resources */
        final long time = entTime - startTime;
        System.out.printf("%d\n", time);
    }

    private static void cleanOutFolder(File out) {
    	 if (!out.exists()) {
             out.mkdirs();
         }

         /* Cleanup */
         File[] contents;
         contents = out.listFiles();
         for (File file : contents) {
             if (file.toString().endsWith(".dcm") || file.toString().endsWith(".bin"))
                 file.delete();
         }
		
	}

	private static void checkInput(String[] args) {
    	if (args.length != 4) {
            System.err.println("Invalid number of arguments");
            printUsage();
            return;
        }
        final String normalize = args[3];
        if (!normalize.equalsIgnoreCase("normalize") && !normalize.equalsIgnoreCase("no-normalize")) {
            System.err.println("Invalid normalize argument: " + normalize);
            printUsage();
            return;
        }
    }
    private static void createDicomObjects(File inputDir) throws IOException {
        if (inputDir.isDirectory()) {
            final File[] dicomFiles = inputDir.listFiles();
            for (File file : dicomFiles) {
                DicomInputStream inputStream = null;
                if (file.toString().endsWith(".dcm")) {
                    try {
                        inputStream = new DicomInputStream(file);
                        inputStream.readDicomObject();
                    } finally {
                        if (inputStream != null)
                            inputStream.close();
                    }
                }
            }
        } else {
            throw new IllegalArgumentException(inputDir + " is not a valid directory");
        }
    }

    private static Study readStudy(File inputDir) throws IOException {
        StudyReader reader = new FileBasedStudyReader(store, inputDir);
        return reader.readStudy();
    }

    private static void writeStudy(Study study, File outputDir) throws IOException {
        File studyVersionFile = new File(outputDir, study.getStudyInstanceUIDAsString() + ".dcm");
        DicomOutputStream dicomOut = new DicomOutputStream(studyVersionFile);
        try {
            dicomOut.writeDicomFile(org.oht.miami.msdtk.conversion.dicom.Study2Dicom
                    .study2MultiSeriesDicom(study));
        } finally {
            dicomOut.close();
        }
    }

    private static Study readFromDicom(File inputDir, File outputDir, boolean normalize)
            throws IOException {
        
        if (inputDir.isDirectory()) {
            StudyReader studyReader = new FileBasedStudyReader(store, inputDir);
            return studyReader.readStudy();
        } else {
            throw new IllegalArgumentException(inputDir + " is not a valid directory");
        }
    }

    // private static Study readFromGPB(File input, File output) throws
    // IOException {
    // Study study = null;
    // InputStream in = new FileInputStream(input);
    // StudyGpb data = StudyGpb.parseFrom(in);
    // study = new Study(store, new DicomUID(), data);
    // return study;
    // }

    private static void printUsage() {
        System.out.println("IntegrationMain [input] [output] [method] [normalize]\n");
        System.out.println("                [method] = clean | read_dicom |  read_dcmObj | write_study_multiframe | write_series_multiframe | write_composite | study_report");
        System.out.println("                [normalize] = normalize | no-normalize");
        System.exit(0);
    }

    /**
     * [MSDTK-105] Creates a function that reports the series within a study. It
     * prints the seriesInstanceUID, that series Instance's and there UID's, and
     * the path to the file for the Instance. All of this information is written
     * to a .txt file
     * 
     * @param inputDir
     *            String path of the study
     * @param outputDir
     *            String path of where to write the .txt report
     * @throws IOException
     */

    public static void createStudyReport(File inputDir, File outputDir) throws IOException {
        /* reads the study */
        StudyReader reader = new FileBasedStudyReader(store, inputDir);
        Study readStudy = reader.readStudy();

        int numSeries = 1;
        int numInstance = 1;
        String fileName = "";

        /* makes sure the outputFilePath exists */

        File reportFile = new File(outputDir,
                readStudy.getValueForAttributeAsString(Tag.StudyInstanceUID) + ".txt");
        FileWriter fWriter = new FileWriter(reportFile);
        BufferedWriter writer = new BufferedWriter(fWriter);

        File[] files = inputDir.listFiles();

        writer.write("StudyInstanceUID:  "
                + readStudy.getValueForAttributeAsString(Tag.StudyInstanceUID) + "\n\n");

        for (Iterator<Series> iter = readStudy.seriesIterator(); iter.hasNext();) {
            Series testSeries = iter.next();
            writer.write("Series #" + numSeries + "\nUID:  " + testSeries.getSeriesInstanceUID()
                    + "\n");
            numSeries++;
            for (Iterator<Instance> instanceIter = testSeries.instanceIterator(); instanceIter
                    .hasNext();) {
                Instance testInstance = instanceIter.next();
                for (File file : files) {
                    DicomInputStream din = new DicomInputStream(new File("" + file));
                    DicomObject dcmObj = din.readDicomObject();
                    din.close();
                    if (testInstance.getSOPInstanceUID().equals(
                            dcmObj.getString(Tag.SOPInstanceUID))) {
                        fileName = file.getName().toString();
                        break;
                    }
                }

                writer.write("\nInstance #" + numInstance + "\nUID:  "
                        + testInstance.getSOPInstanceUID() + "\nFile Location:  " + fileName + "\n");
                numInstance++;
            }
            writer.write("---------------------------------\n");
        }
        writer.close();
    }
}
