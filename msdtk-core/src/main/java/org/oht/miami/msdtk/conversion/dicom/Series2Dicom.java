/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdtk.conversion.dicom;

import org.oht.miami.msdtk.studymodel.BulkDataDicomElement;
import org.oht.miami.msdtk.studymodel.Instance;
import org.oht.miami.msdtk.studymodel.Series;

import org.dcm4che2.data.BasicDicomObject;
import org.dcm4che2.data.DicomElement;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.dcm4che2.data.VR;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Class to convert the study model series object to dcm4che DICOM object.
 * 
 * @author Mahmoud Ismail(maismail@cs.jhu.edu)
 */
public class Series2Dicom {

    public static ArrayList<DicomObject> series2MultiFrameDicom(Series series,
            DicomObject studyDcmObj) throws IOException {

        Iterator<DicomElement> seriesAttrIter;
        Iterator<Instance> instanceIter;
        ArrayList<DicomObject> outputDicomObjects = new ArrayList<DicomObject>();

        // If a series has no instances, return empty list of DicomObjects
        if (series.getInstanceCount() == 0) {
            return outputDicomObjects;
        }
        DicomObject seriesDcmObj = new BasicDicomObject();

        // Adds the study level DICOM element to the series dicom object
        if (studyDcmObj != null) {
            Study2DicomUtil.copyDicomObject(studyDcmObj, seriesDcmObj);
        }

        // Adds all series level attributes on top of base object
        seriesAttrIter = series.attributeIterator();
        while (seriesAttrIter.hasNext()) {
            DicomElement dcmElement = seriesAttrIter.next();
            if (dcmElement.vr() == VR.BD) {
                BulkDataDicomElement bulkDataEle = (BulkDataDicomElement) dcmElement;
                seriesDcmObj.putBytes(dcmElement.tag(), bulkDataEle.getBulkDataVR(),
                        bulkDataEle.getBulkDataValueAsBytes(), bulkDataEle.bigEndian());
            } else {
                seriesDcmObj.add(dcmElement);
            }
        }
        instanceIter = series.instanceIterator();

        // TODO, chooses another SOPInstanceUID other than the
        // SeriesInstanceUID.
        if (series.getInstanceCount() > 1 && studyDcmObj != null) {
            seriesDcmObj.putString(Tag.MediaStorageSOPInstanceUID, VR.UI,
                    series.getSeriesInstanceUID());
            seriesDcmObj.putString(Tag.SOPInstanceUID, VR.UI, series.getSeriesInstanceUID());
        }

        /*
         * If the seiesDcmObj contains the rows and columns dicomElements, this
         * implies all instances within the series have the same size. and the
         * whole series can be converted to a single DicomObject, otherwise each
         * instance will be converted into a separate dicomObject.
         */
        if (series.findAttribute(Tag.Rows) != null && series.findAttribute(Tag.Columns) != null
                && series.findAttribute(Tag.BitsAllocated) != null) {
            seriesDcmObj.putInt(Tag.NumberOfFrames, VR.IS, series.getNumberOfFrames());

            // Read all pixel data associated with the series.
            int frameSize = 1;
            if (series.findAttribute(Tag.Rows) != null) {
                frameSize *= series.findAttribute(Tag.Rows).getInt(false);
            }
            if (series.findAttribute(Tag.Columns) != null) {
                frameSize *= series.findAttribute(Tag.Columns).getInt(false);
            }
            if (series.findAttribute(Tag.BitsAllocated) != null) {
                frameSize *= series.findAttribute(Tag.BitsAllocated).getInt(false) / 8;
            }
            if (series.findAttribute(Tag.SamplesPerPixel) != null) {
                frameSize *= series.findAttribute(Tag.SamplesPerPixel).getInt(false);
            }
            byte[] pixelData = new byte[frameSize * series.getNumberOfFrames()];
            int offset = 0;
            // The sequence dicom element that holds all instances as child
            // frames.z
            DicomElement sequenceElement = seriesDcmObj
                    .putSequence(Tag.PerFrameFunctionalGroupsSequence);
            VR vr = null;
            while (instanceIter.hasNext()) {
                Instance instance = instanceIter.next();
                /*
                 * If the instance does not have the same transfer syntax of the
                 * whole series, skip it
                 */

                ArrayList<DicomObject> dcmObjs = Instance2Dicom
                        .instance2SingleFrameDicomWithNoPixelData(instance, frameSize, offset,
                                pixelData);
                if (vr == null) {
                    vr = ((BulkDataDicomElement) instance.getAttribute(Tag.PixelData))
                            .getBulkDataVR();
                } else {
                    if (vr != ((BulkDataDicomElement) instance.getAttribute(Tag.PixelData))
                            .getBulkDataVR()) {
                        throw new IOException(
                                "Unable to concatenate pixel data with different VRs\n");
                    }
                }
                offset += instance.getNumberOfFrames() * frameSize;
                for (DicomObject dcmObj : dcmObjs) {
                    sequenceElement.addDicomObject(dcmObj);
                }
            }
            seriesDcmObj.putBytes(Tag.PixelData, vr, pixelData);
            outputDicomObjects.add(seriesDcmObj);
        } else {
            while (instanceIter.hasNext()) {
                Instance instance = instanceIter.next();
                outputDicomObjects.add(Instance2Dicom.instance2SingleFrameDicom(seriesDcmObj,
                        instance, false).get(0));
            }
        }
        return outputDicomObjects;
    }
}
