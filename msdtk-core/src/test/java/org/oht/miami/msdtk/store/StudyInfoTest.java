/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdtk.store;

import static org.junit.Assert.assertTrue;

import org.oht.miami.msdtk.store.Store;
import org.oht.miami.msdtk.store.StoreFactory;
import org.oht.miami.msdtk.store.StudyInfo;
import org.oht.miami.msdtk.studymodel.Study;
import org.oht.miami.msdtk.util.DicomUID;

import org.dcm4che2.data.BasicDicomObject;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.dcm4che2.data.VR;
import org.junit.Test;

/**
 * Tests the StudyInfo class
 * 
 * @author rstonebr
 */
public class StudyInfoTest {

    private Store store = StoreFactory.createStore("testStoreFS.properties");

    @Test
    public void testProperties() {

        final String STUDY_UID_VALUE = "1.3.6.1.4.1.5962.1.3.10.2.1166562673.14401";
        final String SERIES_UID_VALUE = "1.3.6.1.4.1.5962.1.3.10.2.1166562673.14403";
        final String SOP_UID_VALUE = "1.3.6.1.4.1.5962.1.1.10.2.1.1166562673.14405";
        final String STUDY_DESCRIPTION = "Study description";
        final String MODALITIES = "PT, CT, MR";
        final String REFERRING_PHYSICIAN = "00000001-0002-0001-0002-000000000005";
        final String PATIENT_ID = "00000001-0002-0001-0002-000000000001";

        DicomObject obj = new BasicDicomObject();
        obj.putString(Tag.StudyInstanceUID, VR.UI, STUDY_UID_VALUE);
        obj.putString(Tag.SeriesInstanceUID, VR.UI, SERIES_UID_VALUE);
        obj.putString(Tag.SOPInstanceUID, VR.UI, SOP_UID_VALUE);
        obj.putString(Tag.StudyDescription, VR.LO, STUDY_DESCRIPTION);
        obj.putString(Tag.ModalitiesInStudy, VR.CS, MODALITIES);
        obj.putString(Tag.PatientID, VR.LO, PATIENT_ID);
        obj.putString(Tag.ReferringPhysicianName, VR.PN, REFERRING_PHYSICIAN);
        DicomUID uid = new DicomUID(STUDY_UID_VALUE);
        Study study = new Study(store, uid, true, obj);
        String studyInfoString = "StudyInfo: " + study.getStudyInstanceUIDAsString() + ", "
                + STUDY_DESCRIPTION;
        StudyInfo studyInfo = new StudyInfo(study);

        assertTrue(studyInfo.toString().equals(studyInfoString));
        assertTrue(studyInfo.getModalities().equals(MODALITIES));
        assertTrue(studyInfo.getNumberOfImages() == 1);
        assertTrue(studyInfo.getNumberOfSeries() == 1);
        assertTrue(studyInfo.getPatientId().toString().equals(PATIENT_ID));
        assertTrue(studyInfo.getReferringPhysician().toString().equals(REFERRING_PHYSICIAN));
    }

}
