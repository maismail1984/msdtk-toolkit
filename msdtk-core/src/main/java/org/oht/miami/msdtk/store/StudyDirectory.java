/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */
package org.oht.miami.msdtk.store;

/**
 * The StudyDirectory is a database that stores a mapping from StudyInstanceUIDs to StudyEntries.
 * This implementation uses a H
 * 
 * @author James F Philbin (james.philbin@jhmi.edu)
 * @author Josh Stephens (joshua.stephens@harris.com)
 * 
 */

import org.oht.miami.msdtk.util.DicomUID;

import java.io.Serializable;
import java.util.HashMap;

public class StudyDirectory implements IStudyDirectory, Serializable {

    private static final long serialVersionUID = 2828214544447627738L;

    private final HashMap<DicomUID, StudyEntry> table = new HashMap<DicomUID, StudyEntry>();

    public final synchronized StudyEntry lookup(DicomUID studyUID) {
        return lookup(studyUID, false);
    }

    public final synchronized StudyEntry lookup(DicomUID studyUID, boolean includeDeleted) {
        StudyEntry entry = table.get(studyUID);
        if (entry == null || entry.deleteType.equals(DeleteType.Live) || includeDeleted) {
            return entry;
        }
        // don't return a deleted entry
        return null;
    }

    public final synchronized void insert(StudyEntry entry) {
        table.put(entry.getStudyInstanceUID(), entry);
    }

    public final synchronized void delete(StudyEntry entry) {
        StudyEntry se = table.remove(entry.getStudyInstanceUID());
        if (se == null) {
            throw new IllegalStateException("The StudyEntry was not found in the Study Directory");
        }
    }

    public synchronized void delete(DicomUID studyInstanceUid) {
        StudyEntry se = table.remove(studyInstanceUid);
        if (se == null) {
            throw new IllegalStateException("The StudyEntry was not found in the Study Directory");
        }
    }

    // TODO implement search
    // public List<StudyInfo> search(String searchCriteria) {
    //
    // }

}
