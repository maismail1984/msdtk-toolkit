/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */
package org.oht.miami.msdtk.store;

/**
 * This exception is called from the StoreFactory when given a wrong Type
 * argument.
 * 
 * @author James F Philbin (james.philbin@jhmi.edu)
 */
public class InvalidStoreTypeException extends IllegalArgumentException {

    static final long serialVersionUID = 1L;

    public InvalidStoreTypeException() {
        super("InvalidStoreTypeException");
    }

    public InvalidStoreTypeException(String s) {
        super(s);
    }

}
