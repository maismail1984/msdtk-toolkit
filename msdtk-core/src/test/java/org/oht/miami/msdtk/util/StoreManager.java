package org.oht.miami.msdtk.util;


import org.oht.miami.msdtk.store.Store;
import org.oht.miami.msdtk.store.StoreFactory;
import org.oht.miami.msdtk.studymodel.Study;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class StoreManager {
    
    // TODO: Determine if the archive will have a single or multiple stores.
    // If multiple stores exist, this might affect the studyDirectory.
    // We have to relate the studyDirectory to the associate store.
    private final ArrayList<Store> stores;
    private Store activeStore = null;

  
    public StoreManager() {
        stores = new ArrayList<Store>();
    }
    
    public void addStoreFS(String rootDirectory) {
        Store store = StoreFactory.createStore(); 
        if(stores.size() == 0) {
            activeStore = store;
        }
        stores.add(store);
    }
    
    public Store getActiveStore() {
        return activeStore;
    }
    
    public void setActiveStore (Store store) {
        activeStore = store;
    }
    
    public void saveStudy (Study study) {
        
        // TODO: The following functions activeStore.appendVersion, activeStore.createVersion() & 
        // activeStore.modifyVersion(version) are to be replaced by activeStore.save.
        // The store has to add the study to the studyEntry
        // studyDirectory.insert(new StudyEntryFS(study));
       // activeStore.save(study);
    }
   
    public Study getStudy(DicomUID studyInstanceUID) throws IOException {
        // Searches for the study in the study directory
        // if exists read the last version from the store.
        return null;
    }
    
    public Study getStudy(DicomUID studyInstanceUID, int vNum) throws IOException {
        // Searches for the study in the study directory
        // if exists read it from the store.
        return null;
    }
    
    public Study readInputStudy(File outputDir) {
        // Call study parser
        // get studyInstance UID
        // Use oldStudy = this.getStudy();
        // read the input study and append it to the oldStudy
        // Return the new version of the study.
        // Note: if oldStudy = null, this implies a new study input.
        // Add to the studyDirectory
        return null;
    }
    
    public void deleteStudy (DicomUID StudyInstanceUID) {
        
    }
    

}
