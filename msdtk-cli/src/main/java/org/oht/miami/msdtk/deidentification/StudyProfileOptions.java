/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdtk.deidentification;

/**
 * Class ProfileOptions. It holds the profile options for a certain study.
 * Initially all are false.
 * 
 * @author mmahmou4
 */
public class StudyProfileOptions {

    /*
     * Basic profile options determines the identification options set for the
     * study
     */
    private boolean isBasicProfile;
    private boolean isRetainSafePrivateOption;
    private boolean isRetainUIDsOption;

    private boolean isRetainDeviceIdentOption;
    private boolean isRetainPatientCharsOption;
    private boolean isRetainLongFullDatesOption;

    private boolean isRetainLongModifiedDatesOption;
    private boolean isCleanDescOption;
    private boolean isCleanStructContOption;
    private boolean isCleanGraphOption;

    /**
     * Constructor to initialize the attribute profile options
     */
    public StudyProfileOptions(boolean basic, boolean rSafePrivate, boolean rUIDOption,
            boolean rDeviceDeidentification, boolean rPatientCharsOptions, boolean rLongFullDates,
            boolean rLongModifiedDate, boolean cDescription, boolean cStructure, boolean cGraphics) {
        isBasicProfile = basic;
        isCleanDescOption = cDescription;
        isCleanGraphOption = cGraphics;
        isCleanStructContOption = cStructure;
        isRetainDeviceIdentOption = rDeviceDeidentification;
        isRetainLongFullDatesOption = rLongFullDates;
        isRetainLongModifiedDatesOption = rLongModifiedDate;
        isRetainPatientCharsOption = rPatientCharsOptions;
        isRetainSafePrivateOption = rSafePrivate;
        isRetainUIDsOption = rUIDOption;
    }

    /* default constructor */
    public StudyProfileOptions() {
        isBasicProfile = true;
    }

    /**
     * @return the isBasicProfile
     */
    public boolean isBasicProfile() {
        return isBasicProfile;
    }

    /**
     * @return the isCleanGraphOption
     */
    public boolean isCleanGraphOption() {
        return isCleanGraphOption;
    }

    /**
     * @return the isCleanStructContOption
     */
    public boolean isCleanStructContOption() {
        return isCleanStructContOption;
    }

    /**
     * @return the isCleanDescOption
     */
    public boolean isCleanDescOption() {
        return isCleanDescOption;
    }

    /**
     * @return the isRetainLongFullDatesOption
     */
    public boolean isRetainLongFullDatesOption() {
        return isRetainLongFullDatesOption;
    }

    /**
     * @return the isRetainLongModifiedDatesOption
     */
    public boolean isRetainLongModifiedDatesOption() {
        return isRetainLongModifiedDatesOption;
    }

    /**
     * @return the isRetainPatientCharsOption
     */
    public boolean isRetainPatientCharsOption() {
        return isRetainPatientCharsOption;
    }

    /**
     * @return the isRetainDeviceIdentOption
     */
    public boolean isRetainDeviceIdentOption() {
        return isRetainDeviceIdentOption;
    }

    /**
     * @return the isRetainUIDsOption
     */
    public boolean isRetainUIDsOption() {
        return isRetainUIDsOption;
    }

    /**
     * @return the isRetainSafePrivateOption
     */
    public boolean isRetainSafePrivateOption() {
        return isRetainSafePrivateOption;
    }

}
